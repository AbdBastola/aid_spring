/**
 * 
 */

var mainApp=angular.module('myApp',['ngMessages']);
mainApp.controller('doctorController',function($scope,$http){
	
	
	$http.get('http://localhost:8080/PatientInformationSystem/viewAllDoctors').
		success(function(data,headers,config,status){
			$scope.doctorsList=data;
		}).error(function(headers,config,status){
			
		});
	
	
	
	
	$scope.isDoctorAvailable=function(event){
		$http({
			method:'GET',
			url:'http://localhost:8080/PatientInformationSystem/isDoctorAvailable/'+event.target.id,
			headers:{'Content-Type':'application/json'}
			
		}).success(function(data,headers,status,config){
			
		}).error(function(headers,status,config){
			
		});
		
		//alert(event.target.id);
		
	};
	
	$scope.isDoctorOff=function(event){
		
		$http({
			method:'GET',
			url:'http://localhost:8080/PatientInformationSystem/isDoctorOff/'+event.target.id,
			headers:{'Content-Type':'application/json'}
		}).success(function(headers,config,data,status){
			
		}).error(function(headers,config,data,status){
			
			
		})
	}
});
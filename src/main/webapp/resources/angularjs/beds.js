/**
 * 
 */

var mainApp=angular.module('myApp',['ngMessages']);

mainApp.controller('bedController',function($scope,$http){
	
	$scope.getFreeBeds=function(){
		
		$http({
			url:'http://localhost:8080/PatientInformationSystem/getAllFreeBeds',
			method:'GET',
			headers:{'Content-Type':'application/json'}
			
		}).success(function(data,status,headers,config){
			$scope.freeBedDatas=data;
		}).error(function(status,headers,config){
			
		});
		
	};
	
	$scope.getOccupiedBeds=function(){
		
		$http({
			url:'http://localhost:8080/PatientInformationSystem/getOccupiedBeds',
			method:'GET',
			headers:{'Content-Type':'application/json'}
				
		}).success(function(data,status,headers,config){
			$scope.occupiedBedDatas=data;
		}).error(function(status,headers,config){
			
		});
	};
	
	
	$http.get('http://localhost:8080/PatientInformationSystem/getAllICUBeds').
		success(function(data,status,headers,config){
			$scope.ICUbeds=data;
		}).error(function(status,headers,config){
			
		});
	
	$http.get('http://localhost:8080/PatientInformationSystem/getAllGeneralWardBeds').
		success(function(data,status,headers,config){
			$scope.generalWardBeds=data;
		}).error(function(status,headers,config){
			
		});
	
	$http.get('http://localhost:8080/PatientInformationSystem/getAllMaternityWardBeds').
		success(function(data,status,headers,config){
			$scope.maternityWardBeds=data;
		}).error(function(status,headers,config){
			
		});
});
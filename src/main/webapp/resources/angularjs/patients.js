/**
 * 
 */

var mainApp=angular.module('myApp',['ngMessages']);

mainApp.controller('addpatientController',function($scope,$http){
	
	$scope.getAllDepartments=function(){
		
		$http({
			
			method:'GET',
			url:'http://localhost:8080/PatientInformationSystem/getDepartments',
			headers:{'Content-Type':'application/json'}
		}).success(function(data,status,headers,config){
			$scope.departmentDatas=data;
		}).error(function(status,headers,config){
			
		});
			
};
	$scope.getAllGenders=function(){
		
		$http({
			method:'GET',
			url:'http://localhost:8080/PatientInformationSystem/getGenders',
			headers:{'Content-Type':'application/json'}
		}).success(function(data,status,headers,config){
			$scope.genderDatas=data;
		}).error(function(status,headers,config){
			
		});
		
	};

	$scope.getAllDoctorsByDepartment=function(){
		
		$http({
			method:'GET',
			url:'http://localhost:8080/PatientInformationSystem/docByDepartment?department='+$scope.departmentmodel,
			headers:{'Content-Type':'application/json'}
			
		}).success(function(data,status,headers,config){
			$scope.doctorsDatas=data
		}).error(function(status,headers,config){
			
		});
	};

});
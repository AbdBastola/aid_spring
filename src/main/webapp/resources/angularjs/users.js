/**
 * 
 */

var mainApp=angular.module('myApp',['ngMessages']);  //bootstrapping the angular project

mainApp.controller('userController',function(){  //defining controller in angular
	
});

mainApp.directive('ngUnique',['$http',function(async){   //defining directive using angular js
	
	return{  //returning 
		
		require:'ngModel',
		link:function(scope,elem,attrs,ctrl){
			elem.on('keyup',function(evt){
				
				scope.$apply(function(){
					var val=elem.val();
					var req={"username":val}  //request sendin to server as a json value
					var ajaxConfiguration={method:'GET',url:'http://localhost:8080/PatientInformationSystem/usernameExists?username='+val,data:req};
					async(ajaxConfiguration).success(function(data,status,headers,config){
						
						ctrl.$setValidity('unique',data);
						console.log(data);
					});
				});
			});
		}
	}
}]);
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html ng-app="myApp">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Add User|Patient Information System</title>
</head>




<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>    
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>  

<% 
//session setting in jsp
String username=(String)session.getAttribute("user");
if(username==null){
	response.sendRedirect(request.getContextPath());
}

%>
 
<!-- Angular JS library -->    
<script src="<c:url value="/resources/lib/angularJS_library/angular.js"/>"></script>
<script src="<c:url value="/resources/lib/angularJS_library/angular.min.js"/>"></script>
<script src="<c:url value="/resources/lib/angularJS_library/angular-messages.js"/>"></script>
<script src="<c:url value="/resources/lib/angularJS_library/angular-messages.min.js"/>"></script>   
 
<script src="<c:url value="/resources/angularjs/users.js"/>"></script>


<!-- BOOTSTRAP STYLES-->
    <link href="<c:url value="/resources/lib/css/bootstrap.css"/>" rel="stylesheet"/>
    
     <!-- FONTAWESOME STYLES-->
    <link href="<c:url value="/resources/lib/css/font-awesome.css"/>" rel="stylesheet"/>
        
     <!-- CUSTOM STYLES-->
    <link href="<c:url value="/resources/lib/css/custom.css"/>" rel="stylesheet"/>
   
    <!-- GOOGLE FONTS-->
   <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
   
   

<body>
     
           
          
    <div id="wrapper">
         <div class="navbar navbar-inverse navbar-fixed-top">
            <div class="adjust-nav">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">
<!--                         <img src="assets/img/logo.png" /> -->
						
                    </a>
                    
                </div>
              
                <span class="logout-spn" >
                  <a href="logout" style="color:#fff;">LOGOUT</a>  

                </span>
            </div>
        </div>
        <!-- /. NAV TOP  -->
        <nav class="navbar-default navbar-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">
                 


                    <li class="active-link">
                        <a href="adminDashBoard" ><i class="fa fa-desktop "></i>Dashboard </a>
                    </li>
                   

                    <li>
                        <a href="addUser"><i class="fa fa-user "></i>Add User</a>
                    </li>
                    <li>
                        <a href="viewUsers"><i class="fa fa-users "></i>View User</a>
                    </li>


                    <li>
                        <a href="viewPatientForAdmin"><i class="fa fa-users "></i>View Patients</a>
                    </li>
                    <li>
                        <a href="viewDoctorForAdmin"><i class="fa fa-user-md"></i>View Doctors</a>
                    </li>

                    <li>
                        <a href="help"><i class="fa fa-envelope "></i>Help</a>
                    </li>
                    <li>
                        <a href="viewDepartmentForAdmin"><i class="fa fa-hospital-o "></i>Departments</a>
                    </li>
                   
                    
                </ul>
                            </div>

        </nav>
        <!-- /. NAV SIDE  -->
        <div id="page-wrapper" >
            <div id="page-inner">
                <div class="row">
                    <div class="col-lg-12">
					<h1>P & T Memorial Hospital</h1>
                     <h2>Register A User</h2> 
						
                    </div>
                </div>              
                 <!-- /. ROW  -->
                  <hr />
                <div class="row" ng-controller="userController">
                    <div class="col-lg-offset-3 col-lg-6 ">
                        <div class="alert alert-info">
                             <strong>Welcome <%= session.getAttribute("user")%> </strong> You can add a new user
                        </div>
                       
               
<!-- /^[a-zA-Z]*$/ -->               
             <form:form action="${pageContext.request.contextPath}/addUser-add" method="POST" modelAttribute="userInfo" required='required' commandName="userInfo" name="addUserForm" class="form-horizontal">
					<label class="control-label" for="firstname">Firstname</label>
                  <div class="controls">
                     <form:input path="firstname" class="form-control" id="firstname" required='required' data-ng-model="firstname" name="firstname" ng-pattern="/^[a-zA-Z]*$/"/>
                      <div ng-messages="addUserForm.firstname.$error" ng-if="addUserForm.firstname.$dirty">
                      <div ng-message='required'>Firstname is required</div>
                      <div ng-message="pattern">Firstname contains only alphabets</div>
                      </div>
                      
                  </div>

                  <label class="control-label" for="lastname">Lastname</label>
                  <div class="controls">
                    <form:input path="lastname" class="form-control" id="lastname" required="required" data-ng-model="lastname" name="lastname" ng-pattern="/^[a-zA-Z]*$/"/>
                     <div ng-messages="addUserForm.lastname.$error" ng-if="addUserForm.lastname.$dirty">
                     	<div ng-message='required'>Lastname is required</div>
                     	<div ng-message="pattern">Lastname contains only alphabets</div>
                     </div>
                  </div>

                   <label class="control-label" for="address">Address</label>
                   <div class="controls">
                     <form:input path="address" id="address" class="form-control" required="required" data-ng-model="address" name="address"/>
                     	<div ng-messages="addUserForm.address.$error" ng-if="addUserForm.address.$dirty">
                     		<div ng-message='required'>Address is required</div>
                     	</div>
                   </div>


                   <label class="control-label" for="inputUsername">Username</label>
                   <div class="controls">
                     <form:input path="username" id="username" class="form-control" required="required" data-ng-model="username" name="username" ng-pattern="/^[_a-z0-9-+]+(\.[_a-z0-9-+]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/" ng-unique="unique"/>
                      <div ng-messages="addUserForm.username.$error" ng-if="addUserForm.username.$dirty">
                      	<div ng-message='required'>Username is required</div>
                      	<div ng-message='unique'>Username Already exists</div>
                      	<div ng-message='pattern'>The email is invalid Please try in this format somebody@gmail.com</div>
                      </div>
                   </div>

                  <label class="control-label" for="inputPassword">Password</label>
                  <div class="controls">
                     <form:input path="password" id="password" class="form-control" required="required" data-ng-model="password" name="password" ng-minlength="6" ng-maxlength="15"/>
                      <div ng-messages="addUserForm.password.$error" ng-if="addUserForm.password.$dirty">
                      	<div ng-message='required'>Password is required</div>
                      	<div ng-message='minlength'>Password should contain at least more than 6 characters</div>
                      	<div ng-message="maxlength">Password should contain less than 15 characters</div>
                      </div>
                  </div>
                  
                  <label class="control-label">Select Roles</label>
                  <div class="controls">
                    <form:select path="roles" items="${roleList}" multiple="true" itemLabel="role_name" itemValue="role_id" class="form-control" required="required" data-ng-model="role" name="role"/>
                  <div ng-messages="addUserForm.role.$error" ng-if="addUserForm.role.$dirty">
                  	<div ng-message='required'>At least one role is required</div>
                  </div>
                  </div>

				
             <br/>
                 <button type="submit" class="btn btn-success" ng-disabled="addUserForm.$invalid">Create new User </button>
 
				</form:form>
					 
					 
					 
				              
               
        
              
                    </div>
                    </div>
                  
               
                
               
              </div>   
              
			 
	
	
             <!-- /. PAGE INNER  -->
            </div>
         <!-- /. PAGE WRAPPER  -->
        </div>
    <div class="footer">
      
    
            <div class="row">
                <div class="col-lg-12" >
                    &copy;  2016 P&T Memorial Hospital |  We care You
                </div>
            </div>
        </div>
          

    
     <!-- /. WRAPPER  -->

       
</body>


    

</html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>    
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>  

<html ng-app="myApp">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Add Doctor|Patient Information System</title>
</head>

<%
String username=(String)session.getAttribute("user");
if(username==null){
	response.sendRedirect(request.getContextPath());
}
%>



	<!-- BOOTSTRAP STYLES-->
    
    <link href="<c:url value="/resources/lib/css/bootstrap.css" />" rel="stylesheet">
     <!-- FONTAWESOME STYLES-->
     <link href="<c:url value="/resources/lib/css/font-awesome.css" />" rel="stylesheet">
   	<!-- CUSTOM STYLES-->
         <link href="<c:url value="/resources/lib/css/custom.css" />" rel="stylesheet">
    
     <!-- GOOGLE FONTS-->
   <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
   
   <!-- angular library -->
    <script src="<c:url value="/resources/lib/angularJS_library/angular.js" />"></script>
    <script src="<c:url value="/resources/lib/angularJS_library/angular.min.js" />"></script>
    <script src="<c:url value="/resources/lib/angularJS_library/angular-resource.js"/>"></script>
   	<script src="<c:url value="/resources/lib/angularJS_library/angular-messages.js"/>"></script>
   	<script src="<c:url value="/resources/lib/angularJS_library/angular-messages.min.js"/>"></script>
   
   <!-- Angular code include in jsp -->
   <script src="<c:url value="/resources/angularjs/patients.js"/>" ></script>

<body ng-controller="addpatientController">
    <div id="wrapper">
         <div class="navbar navbar-inverse navbar-fixed-top">
            <div class="adjust-nav">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">
<!--                         <img src="assets/img/logo.png" /> -->
						
                    </a>
                    
                </div>
              
                <span class="logout-spn" >
              
                  <a href="logout" style="color:#fff;">LOGOUT</a>  

                </span>
            </div>
        </div>
        <!-- /. NAV TOP  -->
        <nav class="navbar-default navbar-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">
                 


                    <li class="active-link">
                        <a href="frontdeskstaffsDashBoard" ><i class="fa fa-desktop "></i>Staff Dashboard </a>
                    </li>
                   

                    <li>
                        <a href="addPatients"><i class="fa fa-user "></i>Add Patients</a>
                    </li>
                    <li>
                        <a href="viewPatients"><i class="fa fa-users "></i>View Patients</a>
                    </li>


                    <li>
                        <a href="addDoctors"><i class="fa fa-user-md "></i>Add Doctors</a>
                    </li>
                    <li>
                        <a href="viewDoctors"><i class="fa fa-user-md"></i>View Doctors</a>
                    </li>
                    
                    
                    <li>
                    <a href="scheduleDoctors"><i class="fa fa-user-md"></i>Schedule Doctors</a>
                    </li>
                    

                    <li>
                        <a href="searchPatientForBedAssign"><i class="fa fa-bed "></i>Assign Beds</a>
                    </li>
                    <li>
                        <a href="departments"><i class="fa fa-hospital-o "></i>Departments</a>
                    </li>
                   
                    <li>
                        <a href="helpStaff"><i class="fa fa-envelope "></i>Help</a>
                    </li>
                   
                </ul>
                            </div>

        </nav>
        <!-- /. NAV SIDE  -->
        <div id="page-wrapper" >
            <div id="page-inner">
                <div class="row">
                    <div class="col-lg-12">
					<h1>P & T Memorial Hospital</h1>
                     <h2>Staff DASHBOARD</h2> 
						
                    </div>
                </div>              
                 <!-- /. ROW  -->
                  <hr />
                <div class="row">
                    <div class="col-lg-offset-3 col-lg-6 ">
                        <div class="alert alert-info">
                             <strong>Welcome <%=session.getAttribute("user")%> ! </strong> You can now add the patients
                        </div>
                       
                       
                   <form:form action="${pageContext.request.contextPath}/addPatients-add" method="GET" name="addPatientForm" required="required" modelAttribute="patientInfo" commandName="patientInfo" class="form-horizontal">
                   		<label class="control-label" for="name">Name</label>
                   		<div class="controls">
                   			<form:input path="name" class="form-control" name="name" data-ng-model="name" required="required" ng-pattern="/^[a-zA-Z\s]*$/"/>
                   			 <div ng-messages="addPatientForm.name.$error" ng-if="addPatientForm.name.$dirty">
                   			 	<div ng-message="required">Full name is required</div>
                   			 	<div ng-message="pattern">Alphabets only please</div>
                   			 </div>
                   		</div>
                   		
                   		<label class="control-label" for="address">Address</label>
                   		<div class="controls">
                   			<form:input path="address" class="form-control" name="address" data-ng-model="address" required="required"/>
                   			 <div ng-messages="addPatientForm.address.$error" ng-if="addPatientForm.address.$dirty">
                   			 	<div ng-message="required">Address is required</div>
                   			 </div>
                   		</div>
                   		
                   		<!-- 
                   		<label class="control-label" for="gender">Select Gender</label>
                  		<div class="controls">
                    		<form:select path="gender" items="${genderList}" itemLabel="gender_name" itemValue="gender_id" class="form-control"/>
   							<p class="help-block">Select one gender at a time</p>	               		
                  		</div>
                   		 -->
                   		 
                   		 <label class="control-label" for="gender">Select Gender</label>
                   			<div class="controls" ng-init="getAllGenders()">
                   				<select name="gender" data-ng-model="gender" class="form-control" name="gender" required="required">
                   					<option value="">--Select Gender--</option>
                   					<option ng-repeat="gender in genderDatas" value="{{gender.gender_id}}">{{gender.gender_name}}</option>
                   				</select>
                   				<div ng-messages="addPatientForm.gender.$error" ng-if="addPatientForm.gender.$dirty">
                   					<div ng-message="required">Please select the gender</div>
                   				</div>
                   			</div>
                   		
                   		
                   		<label class="control-label" for="age">Age</label>
                   		<div class="controls">
                   			<form:input path="age" class="form-control" name="age" data-ng-model="age" required="required" ng-pattern="/^[0-9]+$/"/>
                   			 <div ng-messages="addPatientForm.age.$error" ng-if="addPatientForm.age.$dirty">
                   			 	<div ng-message="required">Age is required</div>
                   			 	<div ng-message="pattern">Age contains only number</div>
                   			 </div>
                   		</div>
                   		
                   		<!--  
                   		<label class="control-label" for="department">Select Department</label>
                  		<div class="controls">
                    		<form:select path="department" items="${departmentList}" itemLabel="department_name" itemValue="dept_id" class="form-control"/>
                  			<p class="help-block">Select only one department at a time</p>
                  		</div>
                   		-->
                   		
                   		<label class="control-label" for="department">Select Department</label>
                  		<div class="controls" ng-init="getAllDepartments()">
                    		<select ng-click="getAllDoctorsByDepartment()" name="department" data-ng-model="departmentmodel" class="form-control" required="required">
                    		<option value="">--Select Department--</option>
                    		<option ng-repeat="department in departmentDatas" value="{{department.dept_id}}">{{department.department_name}}</option>
                    		</select>
                    		<div ng-messages="addPatientForm.department.$error" ng-if="addPatientForm.department.$dirty">
                    			<div ng-message="required">Please select at least one department</div>
                    		</div>
                  		</div>
                   	
                   	
                   	
                   		<label class="control-label" for="doctor">Select Doctor</label>
                   		<div class="controls">
                   			<select name="doctor" ng-model="doctormodel" ng-disabled=!departmentmodel required="required" class="form-control">
                   			<option value="">--Select Doctor--</option>
                   			<option ng-repeat="doctor in doctorsDatas" value="{{doctor.doctor_id}}">Dr.{{doctor.firstname}} {{doctor.lastname}}</option>
                   			</select>
                   			<div ng-messages="addPatientForm.doctor.$error" ng-if="addPatientForm.doctor.$dirty">
                   				<div ng-message="required">Please select the doctor</div>
                   			</div>
                   		</div>
                   	
                   
                   		<!-- 	
                   		<label class="control-label">Select Doctors</label>
                  		<div class="controls">
                    		<form:select path="doctor" items="${doctorsList}" itemLabel="firstname" itemValue="doctor_id" class="form-control"/>
                  			<p class="help-block">Select one doctor at a time</p>
                  		</div>
                   		 -->
                   		
                   		
                   		<br/>
                 <button type="submit" class="btn btn-success" ng-disabled="addPatientForm.$invalid">Save Patient</button>
                   		
                   		
                   </form:form>   
                       
                    </div>
                    
                    
                    </div>
                  
                 <!-- /. ROW  -->  
                 
              
			 
	
	
             <!-- /. PAGE INNER  -->
            </div>
         <!-- /. PAGE WRAPPER  -->
        </div>
    <div class="footer">
      
    
            <div class="row">
                <div class="col-lg-12" >
                    &copy;  2016 P&T Memorial Hospital |  We care You
                </div>
            </div>
        </div>
          

    
     <!-- /. WRAPPER  -->
    <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
    <!-- JQUERY SCRIPTS -->
    <script src="<c:url value="/resources/lib/js/jquery-1.10.2.js" />"></script>
      	<!-- BOOTSTRAP SCRIPTS -->
    <script src="<c:url value="/resources/lib/js/bootstrap.min.js" />"></script>
    	<!-- CUSTOM SCRIPTS -->
    <script src="<c:url value="/resources/lib/js/custom.js" />"></script>
      
   
   
</body>



</html>
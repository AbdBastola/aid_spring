<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html ng-app="myApp">
<head>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
<form action="timepass-add" method="GET">

<div ng-controller="ExampleController">
  
    <label for="repeatSelect"> Repeat select: </label>
    <select name="repeatSelect" id="repeatSelect" ng-model="data.model">
      <option ng-repeat="option in data.availableOptions" value="{{option.id}}">{{option.name}}</option>
    </select>
  
  <hr>
  <tt>model = {{data.model}}</tt><br/>
</div>

<button type="submit">Submit</button>

</form>
</body>



<script>

var mainApp=angular.module('myApp',[]);
mainApp.controller('ExampleController', ['$scope', function($scope) {
  $scope.data = {
   model: null,
   availableOptions: [
     {id: '1', name: 'Option A'},
     {id: '2', name: 'Option B'},
     {id: '3', name: 'Option C'}
   ]
  };
}]);

</script>


</html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>    
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>  

<html ng-app="myApp">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Staffs Dashboard|Patient Information System</title>
</head>

<%
  response.setIntHeader("Refresh", 10); //refresh the page every 10 second
  
  String username=(String)session.getAttribute("user");
  if(username==null){
	  response.sendRedirect(request.getContextPath());
  }
%>

<!-- BOOTSTRAP STYLES-->
    <link href="resources/lib/css/bootstrap.css" rel="stylesheet" />
     <!-- FONTAWESOME STYLES-->
    <link href="resources/lib/css/font-awesome.css" rel="stylesheet" />
        <!-- CUSTOM STYLES-->
    <link href="resources/lib/css/custom.css" rel="stylesheet" />
     <!-- GOOGLE FONTS-->
   <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
   
   
   <!-- Angular JS Library -->
  <script src="<c:url value="/resources/lib/angularJS_library/angular.js"/>"></script>
  <script src="<c:url value="/resources/lib/angularJS_library/angular.min.js"/>"></script>
  <script src="<c:url value="/resources/lib/angularJS_library/angular-resource.js"/>"></script>
   <script src="<c:url value="/resources/lib/angularJS_library/angular-messages.js"/>"></script>
    <script src="<c:url value="/resources/lib/angularJS_library/angular-messages.min.js"/>"></script>
   
   <script src="<c:url value="/resources/angularjs/doctors.js"/>"></script>

<body ng-controller="doctorController">
    <div id="wrapper">
         <div class="navbar navbar-inverse navbar-fixed-top">
            <div class="adjust-nav">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">
<!--                         <img src="assets/img/logo.png" /> -->
						
                    </a>
                    
                </div>
              
                <span class="logout-spn" >
              
                  <a href="logout" style="color:#fff;">LOGOUT</a>  

                </span>
            </div>
        </div>
        <!-- /. NAV TOP  -->
        <nav class="navbar-default navbar-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">
                 


                    <li class="active-link">
                        <a href="frontdeskstaffsDashBoard" ><i class="fa fa-desktop "></i>Staff Dashboard </a>
                    </li>
                   

                    <li>
                        <a href="addPatients"><i class="fa fa-user "></i>Add Patients</a>
                    </li>
                    <li>
                        <a href="viewPatients"><i class="fa fa-users "></i>View Patients</a>
                    </li>


                    <li>
                        <a href="addDoctors"><i class="fa fa-user-md "></i>Add Doctors</a>
                    </li>
                    <li>
                        <a href="viewDoctors"><i class="fa fa-user-md"></i>View Doctors</a>
                    </li>

					
                    <li>
                    <a href="scheduleDoctors"><i class="fa fa-user-md"></i>Schedule Doctors</a>
                    </li>
					

                    <li>
                        <a href="searchPatientForBedAssign"><i class="fa fa-bed "></i>Assign Beds</a>
                    </li>
                    <li>
                        <a href="departments"><i class="fa fa-hospital-o "></i>Departments</a>
                    </li>
                   <li>
                        <a href="helpStaff"><i class="fa fa-envelope "></i>Help</a>
                    </li>
                   
                    
                </ul>
                            </div>

        </nav>
        <!-- /. NAV SIDE  -->
        <div id="page-wrapper" >
            <div id="page-inner">
                <div class="row">
                    <div class="col-lg-12">
					<h1>P & T Memorial Hospital</h1>
                     <h2>Staff DASHBOARD</h2> 
						
                    </div>
                </div>              
                 <!-- /. ROW  -->
                  <hr />
                <div class="row">
                    <div class="col-lg-12 ">
                        <div class="alert alert-info">
                             <strong>Welcome <%=session.getAttribute("user") %> ! </strong>You can now schedule doctors-Staff panel
                        </div>
                       
                       <table class="table table-striped table-hover table-bordered">
                       <thead>
                       <tr>
                       <td>Firstname</td>
                       <td>Lastname</td>
                       <td>Arrival Time</td>
                       <td>Departure Time</td>
                       <td>Status</td>
                       <td>Department</td>
                       <td>Action</td>
                       </tr>
                       </thead>
                       
                       <tbody>
                       <tr ng-repeat="doc in doctorsList">
                       <td>{{doc.firstname}}</td>
                       <td>{{doc.lastname}}</td>
                       <td>{{doc.arrivalTime}}</td>
                       <td>{{doc.departureTime}}</td>
                       <td>{{doc.status}}</td>
                       <td>{{doc.department.department_name}}</td>
                       <td><button class="btn btn-success" id="{{doc.doctor_id}}"  ng-click="isDoctorAvailable($event)">Active</button>|<button class="btn btn-danger" id="{{doc.doctor_id}}"  ng-click="isDoctorOff($event)">Off</button></td>
                       </tr>
                       
                       </tbody>
                       </table>
                       
                       
                    </div>
                    </div>
                  
                 <!-- /. ROW  -->  
                 
	
	                  
             <!-- /. PAGE INNER  -->
            </div>
         <!-- /. PAGE WRAPPER  -->
        </div>
    <div class="footer">
      
    
            <div class="row">
                <div class="col-lg-12" >
                    &copy;  2016 P&T Memorial Hospital |  We care You
                </div>
            </div>
        </div>
          

    
     <!-- /. WRAPPER  -->
    <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
    <!-- JQUERY SCRIPTS -->
    <script src="resources/lib/js/jquery-1.10.2.js"></script>
      <!-- BOOTSTRAP SCRIPTS -->
    <script src="resources/lib/js/bootstrap.min.js"></script>
      <!-- CUSTOM SCRIPTS -->
    <script src="resources/lib/js/custom.js"></script>
   
</body>
</html>
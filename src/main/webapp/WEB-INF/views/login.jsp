<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>    
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>      
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Login Page|Patient Information System</title>
</head>

<link href="<c:url value="/resources/lib/css/bootstrap.css"/>" rel="stylesheet"/>

<link href="<c:url value="/resources/lib/css/font-awesome.css"/>" rel="stylesheet"/>

<link href="<c:url value="/resources/lib/css/custom.css"/>" rel="stylesheet"/>

<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />

<body>

<div class="row">
	<div class="col-lg-offset-3 col-lg-6">
		<div class="alert alert-info">
		<strong>P & T Memorial Hospital Login Page</strong>
		</div>
		
	<c:url var="loginUrl" value="/j_spring_security_check" />

<form:form action="${loginUrl}" method="POST"  modelAttribute="userInfo" class="form-horizontal">

	<c:if test="${param.error!=null}">
	<div>
	<p>Invalid username or password</p>
	</div>
	</c:if>
	
	<c:if test="${param.error!=null}">
	<div>
	<p>You have been logout out successfully from the system</p>
	</div>
	</c:if>

<label class="control-label" for="username">Username</label>
<div class="controls">
	<c:set var="placeholder" value='Enter your username'/>
	<form:input path="username" class="form-control" id="username" placeholder="${placeholder}"/>
</div>


<label class="control-label" for="password">Password</label>
<div class="controls">
	<c:set var="placeholder" value="Enter your password" />
	<form:password path="password" class="form-control" id="password" placeholder="${placeholder}"/>
</div>
<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}">

<br/>
<button type="submit"  class="btn btn-primary">Login</button>

</form:form>

	</div>
</div>

<script src="<c:url value="/resources/lib/js/jquery-1.10.2.js"/>"></script>
    
      <!-- BOOTSTRAP SCRIPTS -->
<script src="<c:url value="/resources/lib/js/bootstrap.min.js"/>"></script>
    
      <!-- CUSTOM SCRIPTS -->
<script src="<c:url value="/resources/lib/js/custom.js"/>"></script>
</body>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>    
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>  
<%@page session="true" %>
<html ng-app="myApp">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Doctor Dashboard|Patient Information System</title>
</head>

<!-- Angular JS library -->
	<script src="<c:url value="/resources/lib/angularJS_library/angular.js"/>"></script>
	<script src="<c:url value="/resources/lib/angularJS_library/angular.min.js"/>"></script>
	<script src="<c:url value="/resources/lib/angularJS_library/angular-messages.js"/>"></script>
	<script src="<c:url value="/resources/lib/angularJS_library/angular-messages.min.js"/>"></script>

<!-- BOOTSTRAP STYLES-->
    <link href="resources/lib/css/bootstrap.css" rel="stylesheet" />
     <!-- FONTAWESOME STYLES-->
    <link href="resources/lib/css/font-awesome.css" rel="stylesheet" />
        <!-- CUSTOM STYLES-->
    <link href="resources/lib/css/custom.css" rel="stylesheet" />
     <!-- GOOGLE FONTS-->
   <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
   
   
   

<body ng-controller="doctorCtrl">
    <div id="wrapper">
         <div class="navbar navbar-inverse navbar-fixed-top">
            <div class="adjust-nav">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">
<!--                         <img src="assets/img/logo.png" /> -->
						
                    </a>
                    
                </div>
              
                <span class="logout-spn" >
              
                  <a href="logout" style="color:#fff;">LOGOUT</a>  

                </span>
            </div>
        </div>
        <!-- /. NAV TOP  -->
        <nav class="navbar-default navbar-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">
                 


                    <li class="active-link">
                        <a href="doctorDashBoard" ><i class="fa fa-desktop "></i>Dashboard </a>
                    </li>
                   

                    <li class="active-link">
                   	<a href="viewPatientInformation"><i class="fa fa-users"></i>View Patients Info</a>
                   </li>
                   
                   <li class="active-link">
                   	<a href="helpDoctor"><i class="fa fa-envelope"></i>Help</a>
                   </li>
                    
                </ul>
                            </div>

        </nav>
        <!-- /. NAV SIDE  -->
        <div id="page-wrapper" >
            <div id="page-inner">
                <div class="row">
                    <div class="col-lg-12">
					<h1>P & T Memorial Hospital</h1>
                     <h2>Doctor DASHBOARD</h2> 
						
                    </div>
                </div>              
                 <!-- /. ROW  -->
                  <hr />
                <div class="row">
                    <div class="col-lg-12 ">
                        <div class="alert alert-info">
                             <strong>Welcome ${userInfo} ! </strong> Doctor panel
                        </div>
                       
                       
                       <form method="GET" action="searchPatient" name="searchPatientForm" class="form-horizontal">
                       
                       <label class="control-label" for="search">Enter id of the patient</label>
                       <div class="controls">
                       <input type="text" name="searchId" id="searchId" name="searchId" data-ng-model="patientSearch" required="required" ng-pattern="/^[0-9]+$/"><button type="submit" class="btn btn-primary" ng-disabled="searchPatientForm.$invalid"><i class="icon-search">Search</i></button>
                       <div ng-messages="searchPatientForm.searchId.$error" ng-if="searchPatientForm.searchId.$dirty">
                       	<div ng-message="required">Patient Id is required</div>
                       	<div ng-message="pattern">Id consists of only numbers</div>
                       </div>
                       </div>
                       
                       
                       </form>
                       
                    </div>
                    </div>
                  
                            <!-- /. PAGE INNER  -->
            </div>
         <!-- /. PAGE WRAPPER  -->
        </div>
    <div class="footer">
      
    
            <div class="row">
                <div class="col-lg-12" >
                    &copy;  2016 P&T Memorial Hospital |  We care You
                </div>
            </div>
        </div>
          

    
     <!-- /. WRAPPER  -->
    <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
    <!-- JQUERY SCRIPTS -->
    <script src="resources/lib/js/jquery-1.10.2.js"></script>
      <!-- BOOTSTRAP SCRIPTS -->
    <script src="resources/lib/js/bootstrap.min.js"></script>
      <!-- CUSTOM SCRIPTS -->
    <script src="resources/lib/js/custom.js"></script>


   
</body>

<script>
var mainApp=angular.module('myApp',['ngMessages']);
mainApp.controller('doctorCtrl',function(){
	
	
});

</script>

</html>
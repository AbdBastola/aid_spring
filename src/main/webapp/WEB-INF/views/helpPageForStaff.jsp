<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>    
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>  

<html ng-app="myApp">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Add Doctor|Patient Information System</title>
</head>

<%
String username=(String)session.getAttribute("user");
if(username==null){
	response.sendRedirect(request.getContextPath());
}
%>


<!-- Angular JS Library -->
<script src="<c:url value="/resources/lib/angularJS_library/angular.js"/>"></script>
<script src="<c:url value="/resources/lib/angularJS_library/angular.min.js"/>"></script>
<script src="<c:url value="/resources/lib/angularJS_library/angular-messages.js"/>"></script>
<script src="<c:url value="/resources/lib/angularJS_library/angular-messages.min.js"/>"></script>

<script src="<c:url value="/resources/angularjs/doctors.js"/>"></script>

<!-- BOOTSTRAP STYLES-->
    <link href="resources/lib/css/bootstrap.css" rel="stylesheet" />
     <!-- FONTAWESOME STYLES-->
    <link href="resources/lib/css/font-awesome.css" rel="stylesheet" />
        <!-- CUSTOM STYLES-->
    <link href="resources/lib/css/custom.css" rel="stylesheet" />
     <!-- GOOGLE FONTS-->
   <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
   
   
   

<body ng-controller="doctorController">
    <div id="wrapper">
         <div class="navbar navbar-inverse navbar-fixed-top">
            <div class="adjust-nav">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">
<!--                         <img src="assets/img/logo.png" /> -->
						
                    </a>
                    
                </div>
              
                <span class="logout-spn" >
              
                  <a href="logout" style="color:#fff;">LOGOUT</a>  

                </span>
            </div>
        </div>
        <!-- /. NAV TOP  -->
        <nav class="navbar-default navbar-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">
                 


                    <li class="active-link">
                        <a href="frontdeskstaffsDashBoard" ><i class="fa fa-desktop "></i>Staff Dashboard </a>
                    </li>
                   

                    <li>
                        <a href="addPatients"><i class="fa fa-user "></i>Add Patients</a>
                    </li>
                    <li>
                        <a href="viewPatients"><i class="fa fa-users "></i>View Patients</a>
                    </li>


                    <li>
                        <a href="addDoctors"><i class="fa fa-user-md "></i>Add Doctors</a>
                    </li>
                    <li>
                        <a href="viewDoctors"><i class="fa fa-user-md"></i>View Doctors</a>
                    </li>
                    
                    
                    <li>
                    <a href="scheduleDoctors"><i class="fa fa-user-md"></i>Schedule Doctors</a>
                    </li>
                    

                    <li>
                        <a href="searchPatientForBedAssign"><i class="fa fa-bed "></i>Assign Beds</a>
                    </li>
                    <li>
                        <a href="departments"><i class="fa fa-hospital-o "></i>Departments</a>
                    </li>
                   
                    <li>
                        <a href="helpStaff"><i class="fa fa-envelope "></i>Help</a>
                    </li>
                </ul>
                            </div>

        </nav>
        <!-- /. NAV SIDE  -->
        <div id="page-wrapper" >
            <div id="page-inner">
                <div class="row">
                    <div class="col-lg-12">
					<h1>P & T Memorial Hospital</h1>
                     <h2>Staff DASHBOARD</h2> 
						
                    </div>
                </div>              
                 <!-- /. ROW  -->
                  <hr />
                <div class="row">
                    <div class="col-lg-offset-3 col-lg-6 ">
                        <div class="alert alert-info">
                             <strong>Welcome <%= session.getAttribute("user")%> ! </strong>Help Page-Staff panel
                        </div>
                       
                  
                  <h3>How to login?</h3>
                  <p>Start the system. You will see the login page with username and password field. Enter the valid username and password to the form. If the login credentials are correct you will be redirected to dashboard otherwise you will be asked to login again by the system</p>
                  
                  <h3>How to add Patients?</h3>
                  <p>After you login you will see the dashboard, click the add patients link where add patients form will appear. Enter the valid information of the patient and select the doctor and department for the patient and click the save patient page where you will be redirected to view patients page </p>
                  
                  <h3>How to view Patients?</h3>
                  <p>Click the view patients link from the dashboard where all the patients of the system will be loaded. You will have button link to update patients form the view patients page</p>
                  
                  <h3>How to update Patients?</h3>
                  <p>From the view patients form click the update link where all the information of selected patient is loaded. Edit the information of the patient and click update patient button to update the patient information where you will be redirected to view patients page</p>
                  
                  <h3>How to add Doctors?</h3>
                  <p>Click the add doctors link from the dashboard where add doctor form will be loaded. Enter all the valid information of the doctor and select department for the doctor. Click the save doctor button where the doctor will be saved to system and you will be redirected to view doctor page</p>
                  
                  <h3>How to view Doctors?</h3>
                  <p>Click the view doctors link from the dashboard where all the doctors of the system will be loaded. You will have permission to update doctor</p>
                  
                   <h3>How to update Doctors?</h3>
                   <p>You will see the update link button in the view doctors page. Click the link to update the doctor. After clicking the update doctor button all the information of selected doctor will be loaded. Edit the information you want to edit and click update doctor button to update the doctor where doctor will be updated. You'll be redirected to view doctor page</p>
                  
                  <h3>How to schedule Doctors?</h3>
                  <p>If the doctor is on duty then click the active button and if the doctor is off duty click the off button to schedule the doctor</p>
                  
                  <h3>How to search patient?</h3>
                  <p>To search the patient, click the assign bed link where search form will appear. Enter the id of the patient to whom bed is to be assigned. Patient Description will be loaded according to patient id</p>
                  
                  <h3>How to assign Beds?</h3>
                  <p>To assign bed, click the assign bed after you search the patient by id. After clicking the assign bed button a form will appear. Select the bed you want to assign and click the assign bed button where bed will be assigned to the patients</p>
                  
                  <h3>How to cancel beds?</h3>
                  <p>To cancel bed, click the cancel bed after you search the patient by id. After clicking the cancel bed button a form will appear. Select the bed you want to cancel and click the cancel bed button where bed will be cancelled to patients</p>
                  
                 
                  <h3>How to view departments?</h3>
                  <p>To view all the departments of the hospitals please click the view departments link present in the dashboard</p>
                  
                  <h3>How to logout from the system?</h3>
                       <p>In every page form dashboard to other there is logout link at the top right corner of the page.Click that link then you will be logout from the system</p>
                  
                  
                    </div>
                    
                    
                    </div>
                  
                 <!-- /. ROW  -->  
                 
              
			 
	
	
             <!-- /. PAGE INNER  -->
            </div>
         <!-- /. PAGE WRAPPER  -->
        </div>
    <div class="footer">
      
    
            <div class="row">
                <div class="col-lg-12" >
                    &copy;  2016 P&T Memorial Hospital |  We care You
                </div>
            </div>
        </div>
          

    
     <!-- /. WRAPPER  -->
    <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
    <!-- JQUERY SCRIPTS -->
    <script src="resources/lib/js/jquery-1.10.2.js"></script>
      <!-- BOOTSTRAP SCRIPTS -->
    <script src="resources/lib/js/bootstrap.min.js"></script>
      <!-- CUSTOM SCRIPTS -->
    <script src="resources/lib/js/custom.js"></script>
   
</body>
</html>
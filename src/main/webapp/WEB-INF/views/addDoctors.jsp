<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>    
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>  

<html ng-app="myApp">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Add Doctor|Patient Information System</title>
</head>

<%
String username=(String)session.getAttribute("user");
if(username==null){
	response.sendRedirect(request.getContextPath());
}
%>


<!-- Angular JS Library -->
<script src="<c:url value="/resources/lib/angularJS_library/angular.js"/>"></script>
<script src="<c:url value="/resources/lib/angularJS_library/angular.min.js"/>"></script>
<script src="<c:url value="/resources/lib/angularJS_library/angular-messages.js"/>"></script>
<script src="<c:url value="/resources/lib/angularJS_library/angular-messages.min.js"/>"></script>

<script src="<c:url value="/resources/angularjs/doctors.js"/>"></script>

<!-- BOOTSTRAP STYLES-->
    <link href="resources/lib/css/bootstrap.css" rel="stylesheet" />
     <!-- FONTAWESOME STYLES-->
    <link href="resources/lib/css/font-awesome.css" rel="stylesheet" />
        <!-- CUSTOM STYLES-->
    <link href="resources/lib/css/custom.css" rel="stylesheet" />
     <!-- GOOGLE FONTS-->
   <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
   
   
   

<body ng-controller="doctorController">
    <div id="wrapper">
         <div class="navbar navbar-inverse navbar-fixed-top">
            <div class="adjust-nav">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">
<!--                         <img src="assets/img/logo.png" /> -->
						
                    </a>
                    
                </div>
              
                <span class="logout-spn" >
              
                  <a href="logout" style="color:#fff;">LOGOUT</a>  

                </span>
            </div>
        </div>
        <!-- /. NAV TOP  -->
        <nav class="navbar-default navbar-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">
                 


                    <li class="active-link">
                        <a href="frontdeskstaffsDashBoard" ><i class="fa fa-desktop "></i>Staff Dashboard </a>
                    </li>
                   

                    <li>
                        <a href="addPatients"><i class="fa fa-user "></i>Add Patients</a>
                    </li>
                    <li>
                        <a href="viewPatients"><i class="fa fa-users "></i>View Patients</a>
                    </li>


                    <li>
                        <a href="addDoctors"><i class="fa fa-user-md "></i>Add Doctors</a>
                    </li>
                    <li>
                        <a href="viewDoctors"><i class="fa fa-user-md"></i>View Doctors</a>
                    </li>
                    
                    
                    <li>
                    <a href="scheduleDoctors"><i class="fa fa-user-md"></i>Schedule Doctors</a>
                    </li>
                    

                    <li>
                        <a href="searchPatientForBedAssign"><i class="fa fa-bed "></i>Assign Beds</a>
                    </li>
                    <li>
                        <a href="departments"><i class="fa fa-hospital-o "></i>Departments</a>
                    </li>
                    
                     <li>
                        <a href="helpStaff"><i class="fa fa-envelope "></i>Help</a>
                    </li>
                   
                    
                </ul>
                            </div>

        </nav>
        <!-- /. NAV SIDE  -->
        <div id="page-wrapper" >
            <div id="page-inner">
                <div class="row">
                    <div class="col-lg-12">
					<h1>P & T Memorial Hospital</h1>
                     <h2>Staff DASHBOARD</h2> 
						
                    </div>
                </div>              
                 <!-- /. ROW  -->
                  <hr />
                <div class="row">
                    <div class="col-lg-offset-3 col-lg-6 ">
                        <div class="alert alert-info">
                             <strong>Welcome <%= session.getAttribute("user")%> ! </strong>You can now add Doctors-Staff panel
                        </div>
                       
                       
                   <form:form action="${pageContext.request.contextPath}/addDoctors-add" method="POST" name="addDoctorForm" modelAttribute="doctorInfo" commandName="doctorInfo" required="required" class="form-horizontal">
                   		<label class="control-label" for="firstname">Firstname</label>
                   		<div class="controls">
                   			<form:input path="firstname" class="form-control" name="firstname" data-ng-model="firstname" required="required" ng-pattern="/^[a-zA-Z]*$/"/>
                   			 <div ng-messages="addDoctorForm.firstname.$error" ng-if="addDoctorForm.firstname.$dirty">
                   			 	<div ng-message="required">Firstname is required</div>
                   			 	<div ng-message="pattern">Firstname contains Alphabets only</div>
                   			 </div>
                   		</div>
                   		
                   		<label class="control-label" for="lastname">Lastname</label>
                   		<div class="controls">
                   			<form:input path="lastname" class="form-control" name="lastname" data-ng-model="lastname" required="required" ng-pattern="/^[a-zA-Z]*$/"/>
                   			 <div ng-messages="addDoctorForm.lastname.$error" ng-if="addDoctorForm.lastname.$dirty">
                   			 	<div ng-message="required">Lastname is required</div>
                   			 	<div ng-message="pattern">Lastname contains Alphabets only</div>
                   			 </div>
                   		</div>
                   		
                   		<label class="control-label" for="specialization">Specialization</label>
                   		<div class="controls">
                   			<form:input path="specialization" class="form-control" name="specialization" data-ng-model="specialization" required="required" ng-pattern="/^[a-zA-Z\s]*$/"/>
                   			 <div ng-messages="addDoctorForm.specialization.$error" ng-if="addDoctorForm.specialization.$dirty">
                   			 	<div ng-message="required">Specialization is required</div>
                   			 	<div ng-message="pattern">Specialization contains Alphabets only</div>
                   			 </div>
                   		</div>
                   		
                   		<label class="control-label" for="degree">Degree</label>
                   		<div class="controls">
                   			<form:input path="degree" class="form-control" name="degree" data-ng-model="degree" required="required" ng-pattern="/^[a-zA-Z\s]*$/"/>
                   			 <div ng-messages="addDoctorForm.degree.$error" ng-if="addDoctorForm.degree.$dirty">
                   			 	<div ng-message="required">Degree is required</div>
                   			 	<div ng-message="pattern">Degree contains only Alphabets only</div>
                   			 </div>
                   		</div>
                   		
                   		<label class="control-label" for="arrivalTime">Arrival Time</label>
                   		<div class="controls">
                   			<form:input path="arrivalTime" class="form-control" name="arrivalTime" data-ng-model="arrivalTime" required="required"/>
                   			<div ng-messages="addDoctorForm.arrivalTime.$error" ng-if="addDoctorForm.arrivalTime.$dirty">
                   				<div ng-message="required">Arrival Time is required</div>
                   			</div>
                   			 <p class="help-block">ArrivalTime can contain only numbers in the format of 12:00</p>
                   		</div>
                   		
                   		<label class="control-label" for="departureTime">Departure Time</label>
                   		<div class="controls">
                   			<form:input path="departureTime" class="form-control" name="departureTime" data-ng-model="departureTime" required="required"/>
                   			<div ng-messages="addDoctorForm.departureTime.$error" ng-if="addDoctorForm.departureTime.$dirty">
                   				<div ng-message="required">Departure Time is required</div>
                   			</div>
                   			<p class="help-block">DepartureTime can contain only numbers in the format of 12:00</p>
                   		</div>
                   		
                   		<label class="control-label">Select Department</label>
                  		<div class="controls">
                    		<form:select path="department" items="${departmentList}" itemLabel="department_name" itemValue="dept_id" class="form-control"/>
                  		</div>
                   		
                   		<br/>
                 <button type="submit" class="btn btn-success" ng-disabled="addDoctorForm.$invalid">Save Doctor </button>
                   		
                   		
                   </form:form>   
                       
                    </div>
                    
                    
                    </div>
                  
                 <!-- /. ROW  -->  
                 
              
			 
	
	
             <!-- /. PAGE INNER  -->
            </div>
         <!-- /. PAGE WRAPPER  -->
        </div>
    <div class="footer">
      
    
            <div class="row">
                <div class="col-lg-12" >
                    &copy;  2016 P&T Memorial Hospital |  We care You
                </div>
            </div>
        </div>
          

    
     <!-- /. WRAPPER  -->
    <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
    <!-- JQUERY SCRIPTS -->
    <script src="resources/lib/js/jquery-1.10.2.js"></script>
      <!-- BOOTSTRAP SCRIPTS -->
    <script src="resources/lib/js/bootstrap.min.js"></script>
      <!-- CUSTOM SCRIPTS -->
    <script src="resources/lib/js/custom.js"></script>
   
</body>
</html>
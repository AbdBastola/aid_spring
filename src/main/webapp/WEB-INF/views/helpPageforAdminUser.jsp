<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Add User|Patient Information System</title>
</head>




<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>    
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>  

<% 
//session setting in jsp
String username=(String)session.getAttribute("user");
if(username==null){
	response.sendRedirect(request.getContextPath());
}

%>
 

<!-- BOOTSTRAP STYLES-->
    <link href="<c:url value="/resources/lib/css/bootstrap.css"/>" rel="stylesheet"/>
    
     <!-- FONTAWESOME STYLES-->
    <link href="<c:url value="/resources/lib/css/font-awesome.css"/>" rel="stylesheet"/>
        
     <!-- CUSTOM STYLES-->
    <link href="<c:url value="/resources/lib/css/custom.css"/>" rel="stylesheet"/>
   
    <!-- GOOGLE FONTS-->
   <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
   
   

<body>
     
           
          
    <div id="wrapper">
         <div class="navbar navbar-inverse navbar-fixed-top">
            <div class="adjust-nav">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">
<!--                         <img src="assets/img/logo.png" /> -->
						
                    </a>
                    
                </div>
              
                <span class="logout-spn" >
                  <a href="logout" style="color:#fff;">LOGOUT</a>  

                </span>
            </div>
        </div>
        <!-- /. NAV TOP  -->
        <nav class="navbar-default navbar-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">
                 


                    <li class="active-link">
                        <a href="adminDashBoard" ><i class="fa fa-desktop "></i>Dashboard </a>
                    </li>
                   

                    <li>
                        <a href="addUser"><i class="fa fa-user "></i>Add User</a>
                    </li>
                    <li>
                        <a href="viewUsers"><i class="fa fa-users "></i>View User</a>
                    </li>


                    <li>
                        <a href="viewPatientForAdmin"><i class="fa fa-users "></i>View Patients</a>
                    </li>
                    <li>
                        <a href="viewDoctorForAdmin"><i class="fa fa-user-md"></i>View Doctors</a>
                    </li>

                    <li>
                        <a href="help"><i class="fa fa-envelope "></i>Help</a>
                    </li>
                    <li>
                        <a href="viewDepartmentForAdmin"><i class="fa fa-hospital-o "></i>Departments</a>
                    </li>
                   
                    
                </ul>
                            </div>

        </nav>
        <!-- /. NAV SIDE  -->
        <div id="page-wrapper" >
            <div id="page-inner">
                <div class="row">
                    <div class="col-lg-12">
					<h1>P & T Memorial Hospital</h1>
                     <h2>Register A User</h2> 
						
                    </div>
                </div>              
                 <!-- /. ROW  -->
                  <hr />
                <div class="row">
                    <div class="col-lg-offset-3 col-lg-6 ">
                        <div class="alert alert-info">
                             <strong>Welcome <%= session.getAttribute("user")%> </strong> Help Page-Admin Panel
                        </div>
           
               
        
        			<h3>How to Login?</h3>
        			<p>A login form will appear after the system starts. Enter the login credentials such as username and password. Click the login button and if login credentials are correct you will be directed to dashboard otherwise you will be again asked to enter the login credentials</p>
        
        			<h3>How to add User?</h3>
        			<p>To add a user you will see the link and icon of add user in dashboard. click the link where add user form will pop up. Enter all the valid details of the user and click add user button. User will be inserted to the system and you will be redirected to view user page</p>
        
        			<h3>How to view User?</h3>
        			<p>To view user click the view user link where all the users of a system will be there. There is update button also. If you want to update a respective user click the update icon button where you will be redirected to update user form where you can edit the user information</p>
        			
        			<h3>How to update User?</h3>
        			<p>After clicking update icon from view user page you will be redirected to update user page where all the information of the selected user is loaded. Edit the information you want to edit and click update button to update user information. You will be redirected to view user page</p>
        
        			<h3>How to view Patients?</h3>
        			<p>To view patients of the system click the view patients link from the dashboard where all the patients of the system will be loaded</p>
        
        			<h3>How to view Doctors?</h3>
        			<p>To view doctors of the system click the view doctors link from the dashboard where all the doctors of the system will be loaded</p>
        
         			<h3>How to view Department?</h3>
        			<p>TO view department of the system click the view department link from the dashboard where all the departments of the system will be loaded</p>
              
              		<h3>How to logout from the system?</h3>
                    <p>In every page form dashboard to other there is logout link at the top right corner of the page.Click that link then you will be logout from the system</p>	
              
                    </div>
                    </div>
                  
               
                
               
              </div>   
              
			 
	
	
             <!-- /. PAGE INNER  -->
            </div>
         <!-- /. PAGE WRAPPER  -->
        </div>
    <div class="footer">
      
    
            <div class="row">
                <div class="col-lg-12" >
                    &copy;  2016 P&T Memorial Hospital |  We care You
                </div>
            </div>
        </div>
          

    
     <!-- /. WRAPPER  -->

       
</body>


    

</html>
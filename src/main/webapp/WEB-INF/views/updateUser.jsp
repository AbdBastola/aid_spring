<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html ng-app="myApp">
<head>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Update User|Patient Information System</title>
</head>



<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>    
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>  

<%
String username=(String)session.getAttribute("user");
if(username==null){
	response.sendRedirect(request.getContextPath());
}
%>

<!-- BOOTSTRAP STYLES-->
    <link href="<c:url value="/resources/lib/css/bootstrap.css"/>" rel="stylesheet"/>
    
     <!-- FONTAWESOME STYLES-->
    <link href="<c:url value="/resources/lib/css/font-awesome.css"/>" rel="stylesheet"/>
        
     <!-- CUSTOM STYLES-->
    <link href="<c:url value="/resources/lib/css/custom.css"/>" rel="stylesheet"/>
   
    <!-- GOOGLE FONTS-->
   <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
   
   <!-- angular JS library -->
   <script src="<c:url value="/resources/lib/angularJS_library/angular.js"/>"></script>
   <script src="<c:url value="/resources/lib/angularJS_library/angular.min.js"/>"></script>
   <script src="<c:url value="/resources/lib/angularJS_library/angular-messages.js"/>"></script>
   <script src="<c:url value="/resources/lib/angularJS_library/angular-messages.min.js"/>"></script>
	<script src="<c:url value="/resources/angularjs/users.js"/>"></script>

<body ng-controller="userController">
     
           
          
    <div id="wrapper">
         <div class="navbar navbar-inverse navbar-fixed-top">
            <div class="adjust-nav">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">
<!--                         <img src="assets/img/logo.png" /> -->
						
                    </a>
                    
                </div>
              
                <span class="logout-spn" >
                  <a href="logout" style="color:#fff;">LOGOUT</a>  

                </span>
            </div>
        </div>
        <!-- /. NAV TOP  -->
        <nav class="navbar-default navbar-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">
                 


                    <li class="active-link">
                        <a href="${pageContext.request.contextPath}/adminDashBoard" ><i class="fa fa-desktop "></i>Dashboard </a>
                    </li>
                   

                    <li>
                        <a href="${pageContext.request.contextPath}/addUser"><i class="fa fa-user "></i>Add User</a>
                    </li>
                    <li>
                        <a href="${pageContext.request.contextPath}/viewUsers"><i class="fa fa-users "></i>View User</a>
                    </li>


                    <li>
                        <a href="${pageContext.request.contextPath}/viewPatientForAdmin"><i class="fa fa-users "></i>View Patients</a>
                    </li>
                    <li>
                        <a href="${pageContext.request.contextPath}/viewDoctorForAdmin"><i class="fa fa-user-md"></i>View Doctors</a>
                    </li>

                    <li>
                        <a href="${pageContext.request.contextPath}/help"><i class="fa fa-envelope "></i>Help</a>
                    </li>
                    <li>
                        <a href="viewDepartmentForAdmin"><i class="fa fa-hospital-o "></i>Departments</a>
                    </li>
                   
                    
                </ul>
                            </div>

        </nav>
        <!-- /. NAV SIDE  -->
        <div id="page-wrapper" >
            <div id="page-inner">
                <div class="row">
                    <div class="col-lg-12">
					<h1>P & T Memorial Hospital</h1>
                     <h2>Register A User</h2> 
						
                    </div>
                </div>              
                 <!-- /. ROW  -->
                  <hr />
                <div class="row">
                    <div class="col-lg-offset-3 col-lg-6 ">
                        <div class="alert alert-info">
                             <strong>Welcome <%=session.getAttribute("user") %>!</strong> You can now update current user
                        </div>
                       
               
             
                       
              <form:form action="${pageContext.request.contextPath}/updateUser-update" method="POST" name="updateUserForm" modelAttribute="userInfo" commandName="userInfo" required="required" class="form-horizontal">


					<form:hidden path="user_id" />

                  <label class="control-label" for="firstname">Firstname</label>
                  <div class="controls">
                     <form:input path="firstname" class="form-control" name="firstname" required="required" />
                    
                  </div>

                  <label class="control-label" for="lastname">Lastname</label>
                  <div class="controls">
                    <form:input path="lastname" class="form-control" id="lastname" required="required"/>
                   
                  </div>

                   <label class="control-label" for="address">Address</label>
                   <div class="controls">
                     <form:input path="address" id="address" class="form-control" required="required"/>
                     
                   </div>


                   <label class="control-label" for="inputUsername">Username</label>
                   <div class="controls">
                     <form:input path="username" id="username" class="form-control" required="required" />
                   
                   </div>

                  <label class="control-label" for="inputPassword">Password</label>
                  <div class="controls">
                     <form:input path="password" id="password" class="form-control" required="required"/>
                      
                  </div>
                  
                  <label class="control-label">Select Roles</label>
                  <div class="controls">
                    <form:select path="roles" items="${roleList}" multiple="true" itemLabel="role_name" itemValue="role_id" required="required" class="form-control"/>
                  </div>


             <br/>
                 <button type="submit" class="btn btn-success">Update User</button>
               <!--   <button type="submit" class="btn btn-primary">Update User</button> -->
               </form:form>
                       
                       
                    </div>
                    </div>
                  
               
                
               
              </div>   
              
			 
	
	
             <!-- /. PAGE INNER  -->
            </div>
         <!-- /. PAGE WRAPPER  -->
        </div>
    <div class="footer">
      
    
            <div class="row">
                <div class="col-lg-12" >
                    &copy;  2016 P&T Memorial Hospital |  We care You
                </div>
            </div>
        </div>
          

    
     <!-- /. WRAPPER  -->
     
    <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
    <!-- JQUERY SCRIPTS -->
   <script src="<c:url value="/resources/lib/js/jquery-1.10.2.js"/>"></script>
    
      <!-- BOOTSTRAP SCRIPTS -->
    <script src="<c:url value="/resources/lib/js/bootstrap.min.js"/>"></script>
    
      <!-- CUSTOM SCRIPTS -->
    <script src="<c:url value="/resources/lib/js/custom.js"/>"></script>
    
   
</body>
<script>

var mainApp=angular.module('myApp',['ngMessages']);
mainApp.controller('userController',function(){
	
	
});
</script>

</html>
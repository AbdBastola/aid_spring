<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>    
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>  
<%@page session="true" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Add Patient Information|Patient Information System</title>
</head>

	<%
		String username=(String)session.getAttribute("user");
	if(username==null){
		response.sendRedirect(request.getContextPath());
	}
	
	%>

<!-- BOOTSTRAP STYLES-->
    <link href="resources/lib/css/bootstrap.css" rel="stylesheet" />
     <!-- FONTAWESOME STYLES-->
    <link href="resources/lib/css/font-awesome.css" rel="stylesheet" />
        <!-- CUSTOM STYLES-->
    <link href="resources/lib/css/custom.css" rel="stylesheet" />
     <!-- GOOGLE FONTS-->
   <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
   
   
   

<body>
    <div id="wrapper">
         <div class="navbar navbar-inverse navbar-fixed-top">
            <div class="adjust-nav">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">
<!--                         <img src="assets/img/logo.png" /> -->
						
                    </a>
                    
                </div>
              
                <span class="logout-spn" >
              
                  <a href="logout" style="color:#fff;">LOGOUT</a>  

                </span>
            </div>
        </div>
        <!-- /. NAV TOP  -->
        <nav class="navbar-default navbar-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">
                 


                    <li class="active-link">
                        <a href="doctorDashBoard" ><i class="fa fa-desktop "></i>Dashboard </a>
                    </li>
                   
                   <li class="active-link">
                   	<a href="viewPatientInformation"><i class="fa fa-users"></i>View Patients Info</a>
                   </li>

                   <li class="active-link">
                   	<a href="helpDoctor"><i class="fa fa-envelope"></i>Help</a>
                   </li>
                   
                    
                </ul>
                            </div>

        </nav>
        <!-- /. NAV SIDE  -->
        <div id="page-wrapper" >
            <div id="page-inner">
                <div class="row">
                    <div class="col-lg-12">
					<h1>P & T Memorial Hospital</h1>
                     <h2>Doctor DASHBOARD</h2> 
						
                    </div>
                </div>              
                 <!-- /. ROW  -->
                  <hr />
                <div class="row">
                    <div class="col-lg-offset-3 col-lg-6 ">
                        <div class="alert alert-info">
                             <strong>Welcome <%=session.getAttribute("user") %> ! </strong>You can now add the information of following patient Doctor Panel
                        </div>
                       
                       
                       
                      
                  <form:form method="POST" action="patientInfo-add" commandName="patientInformation" modelAttribute="patientInformation" class="form-horizontal">
                       <h2>Patient Description</h2>
                       <input type="hidden" value="${patientInfo.getPatient_id()}" name="patientId">
                       <label class="control-label">Patient Name</label><input name="patientname" value="${patientInfo.getName()}" disabled="disabled" class="form-control">
                       <label class="control-label">Age/Sex</label><input name="patientgenderAge" value="${patientInfo.getAge()}/${patientInfo.getGender().getGender_name()}" disabled="disabled" class="form-control">
                       <hr/>
                       
                       <h2>Physical Exam</h2>
                       
                       <label class="control-label">Weight</label>
                       <div class="controls">
                       <form:input path="weight" class="form-control"/>
                       <p class="help-block">Weight in kgs</p>
                       </div>
                       
                       
                       <label class="control-label">Height</label>
                       <div class="controls">
                       <form:input path="height" class="form-control"/>
                       <p class="help-block">Height in ft</p>
                       </div>
                       
                       
                       <label class="control-label">Temperature</label>
                       <div class="controls">
                       <form:input path="temperature" class="form-control"/>
                       <p class="help-block">Temperature in degree celsius</p>
                       </div>
                       
                       
                       <label class="control-label">Pulse</label>
                       <div class="controls">
                       <form:input path="pulse" class="form-control"/>
                       <p class="help-block">Pulse in beats per minute</p>
                       </div>
                       
                       
                       <label class="control-label">Blood Pressure</label>
                       <div class="controls">
                       <form:input path="bloodPressure" class="form-control"/>
                       <p class="help-block">Blood Pressure in mm of HG should include both systolic and distolyic</p>
                       </div>
                       <hr/>
                       
                        <h2>History and Physical Assessment</h2>
                       
                       <label class="label-control">Chief Complaints</label>
                       <div class="controls">
                       <form:textarea path="chiefComplaints" class="form-control"/>
                       </div>
                       
                       <label class="label-control">Provisional Diagnosis</label>
                       <div class="controls">
                       <form:textarea path="provisionalDiagnosis" class="form-control"/>
                       </div>
                       
                       <label class="label-control">Clinical Findings</label>
                       <div class="controls">
                       <form:textarea path="clinicalFindings" class="form-control"/>
                       </div>
                       
                       <br/>
                       <button class="btn btn-success" type="submit">Submit</button>
                       </form:form>
                       
                    </div>
                    </div>
                  
                            <!-- /. PAGE INNER  -->
            </div>
         <!-- /. PAGE WRAPPER  -->
        </div>
    <div class="footer">
      
    
            <div class="row">
                <div class="col-lg-12" >
                    &copy;  2016 P&T Memorial Hospital |  We care You
                </div>
            </div>
        </div>
          

    
     <!-- /. WRAPPER  -->
    <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
    <!-- JQUERY SCRIPTS -->
    <script src="resources/lib/js/jquery-1.10.2.js"></script>
      <!-- BOOTSTRAP SCRIPTS -->
    <script src="resources/lib/js/bootstrap.min.js"></script>
      <!-- CUSTOM SCRIPTS -->
    <script src="resources/lib/js/custom.js"></script>
   
</body>
</html>
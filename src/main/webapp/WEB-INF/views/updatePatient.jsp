<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>    
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>  

<html ng-app="myApp">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Add Doctor|Patient Information System</title>
</head>

<!-- BOOTSTRAP STYLES-->
    <link href="<c:url value="/resources/lib/css/bootstrap.css" />" rel="stylesheet">
     <!-- FONTAWESOME STYLES-->
    <link href="<c:url value="/resources/lib/css/font-awesome.css" />" rel="stylesheet">
        <!-- CUSTOM STYLES-->
    <link href="<c:url value="/resources/lib/css/custom.css" />" rel="stylesheet">
     <!-- GOOGLE FONTS-->
   <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
   
 <!-- angular library -->
    <script src="<c:url value="/resources/lib/angularJS_library/angular.js" />"></script>
    <script src="<c:url value="/resources/lib/angularJS_library/angular.min.js" />"></script>
    <script src="<c:url value="/resources/lib/angularJS_library/angular-resource.js"/>"></script>
     <script src="<c:url value="/resources/lib/angularJS_library/angular-messages.js" />"></script>
    <script src="<c:url value="/resources/lib/angularJS_library/angular-messages.min.js"/>"></script>
   
   <!-- Angular code include in jsp -->
   <script src="<c:url value="/resources/angularjs/patients.js"/>" ></script>

	
	<%
	String username=(String)session.getAttribute("user");
	if(username==null){
		response.sendRedirect(request.getContextPath());
	}
	
	%>

<body ng-controller="addpatientController">
    <div id="wrapper">
         <div class="navbar navbar-inverse navbar-fixed-top">
            <div class="adjust-nav">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">
<!--                         <img src="assets/img/logo.png" /> -->
						
                    </a>
                    
                </div>
              
                <span class="logout-spn" >
              
                  <a href="logout" style="color:#fff;">LOGOUT</a>  

                </span>
            </div>
        </div>
        <!-- /. NAV TOP  -->
        <nav class="navbar-default navbar-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">
                 


                    <li class="active-link">
                        <a href="${pageContext.request.contextPath}/frontdeskstaffsDashBoard" ><i class="fa fa-desktop "></i>Staff Dashboard </a>
                    </li>
                   

                    <li>
                        <a href="${pageContext.request.contextPath}/addPatients"><i class="fa fa-user "></i>Add Patients</a>
                    </li>
                    <li>
                        <a href="${pageContext.request.contextPath}/viewPatients"><i class="fa fa-users "></i>View Patients</a>
                    </li>


                    <li>
                        <a href="${pageContext.request.contextPath}/addDoctors"><i class="fa fa-user-md "></i>Add Doctors</a>
                    </li>
                    <li>
                        <a href="${pageContext.request.contextPath}/viewDoctors"><i class="fa fa-user-md"></i>View Doctors</a>
                    </li>

                    <li>
                        <a href="${pageContext.request.contextPath}/assignBed"><i class="fa fa-bed "></i>Assign Beds</a>
                    </li>
                    <li>
                        <a href="${pageContext.request.contextPath}/departments"><i class="fa fa-hospital-o "></i>Departments</a>
                    </li>
                   <li>
                        <a href="${pageContext.request.contextPath}/helpStaff"><i class="fa fa-envelope "></i>Help</a>
                    </li>
                   
                    
                </ul>
                            </div>

        </nav>
        <!-- /. NAV SIDE  -->
        <div id="page-wrapper" >
            <div id="page-inner">
                <div class="row">
                    <div class="col-lg-12">
					<h1>P & T Memorial Hospital</h1>
                     <h2>Staff DASHBOARD</h2> 
						
                    </div>
                </div>              
                 <!-- /. ROW  -->
                  <hr />
                <div class="row">
                    <div class="col-lg-offset-3 col-lg-6 ">
                        <div class="alert alert-info">
                             <strong>Welcome <%=session.getAttribute("user") %> ! </strong>You can update the patient- Staff panel
                        </div>
                       
                       
                   <form:form action="${pageContext.request.contextPath}/updatePatient-update" method="POST" modelAttribute="patientInfo" commandName="patientInfo" class="form-horizontal">
                   		
                   		<form:hidden path="patient_id"/>
                   		
                   		
                   		<label class="control-label" for="name">Name</label>
                   		<div class="controls">
                   			<form:input path="name" class="form-control" id="name" required="required"/>
                   			 <p class="help-block">Name can contain any letters, without spaces</p>
                   		</div>
                   		
                   		<label class="control-label" for="address">Address</label>
                   		<div class="controls">
                   			<form:input path="address" class="form-control" id="address" required="required"/>
                   			 <p class="help-block">Address can contain any letters, without spaces</p>
                   		</div>
                   		
                   		
                   		
                   		 <label class="control-label" for="gender">Select Gender</label>
                   			<div class="controls" ng-init="getAllGenders()">
                   				<select name="gender" ng-model="genderDatas.model" class="form-control">
                   					<option value="">--Select Gender--</option>
                   					<option ng-repeat="gender in genderDatas" value="{{gender.gender_id}}">{{gender.gender_name}}</option>
                   				</select>
                   				<p class="help-block">Select one gender at a time</p>
                   			</div>
                   		
                   		<!--  
                   		<label class="control-label" for="gender">Select Gender</label>
                  		<div class="controls">
                    		<form:select path="gender" items="${genderList}" itemLabel="gender_name" itemValue="gender_id" class="form-control"/>
   							<p class="help-block">Select one gender at a time</p>	               		
                  		</div>
                   		
                   		-->
                   		
                   		<label class="control-label" for="age">Age</label>
                   		<div class="controls">
                   			<form:input path="age" class="form-control" id="age" required="required"/>
                   			 <p class="help-block">Age can contain only numbers, without spaces</p>
                   		</div>
                   		
                   		
                   		<label class="control-label" for="department">Select Department</label>
                  		<div class="controls" ng-init="getAllDepartments()">
                    		<select ng-click="getAllDoctorsByDepartment()" name="department" ng-model="departmentmodel" class="form-control">
                    		<option value="">--Select Department--</option>
                    		<option ng-repeat="department in departmentDatas" value="{{department.dept_id}}">{{department.department_name}}</option>
                    		</select>
                    		<p class="help-block">Select only one department at a time</p>
                  		</div>
                   	
                   		
                   		<!-- 
                   		<label class="control-label" for="department">Select Department</label>
                  		<div class="controls">
                    		<form:select path="department" items="${departmentList}" itemLabel="department_name" itemValue="dept_id" class="form-control"/>
                  			<p class="help-block">Select only one department at a time</p>
                  		</div>
                   		
                   		
                   		
                   		<label class="control-label">Select Doctors</label>
                  		<div class="controls">
                    		<form:select path="doctor" items="${doctorsList}" itemLabel="firstname" itemValue="doctor_id" class="form-control"/>
                  			<p class="help-block">Select one doctor at a time</p>
                  		</div>
                   		 -->
                   		 
                   		 <label class="control-label" for="doctor">Select Doctor</label>
                   		<div class="controls">
                   			<select name="doctor" ng-model="doctormodel" ng-disabled=!departmentmodel class="form-control">
                   			<option value="">--Select Doctor--</option>
                   			<option ng-repeat="doctor in doctorsDatas" value="{{doctor.doctor_id}}">Dr.{{doctor.firstname}} {{doctor.lastname}}</option>
                   			</select>
                   			<p class="help-block">Select only one doctor at a time</p>
                   		</div>
                   	
                   	
                   		
                   		
                   		<br/>
                 <button type="submit" class="btn btn-success">Update Patient</button>
                   		
                   		
                   </form:form>   
                       
                    </div>
                    
                    
                    </div>
                  
                 <!-- /. ROW  -->  
                 
              
			 
	
	
             <!-- /. PAGE INNER  -->
            </div>
         <!-- /. PAGE WRAPPER  -->
        </div>
    <div class="footer">
      
    
            <div class="row">
                <div class="col-lg-12" >
                    &copy;  2016 P&T Memorial Hospital |  We care You
                </div>
            </div>
        </div>
          

    
     <!-- /. WRAPPER  -->
    <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
    <!-- JQUERY SCRIPTS -->
    <script src="<c:url value="/resources/lib/js/jquery-1.10.2.js"/>"></script>
      <!-- BOOTSTRAP SCRIPTS -->
    <script src="<c:url value="/resources/lib/js/bootstrap.min.js"/>"></script>
      <!-- CUSTOM SCRIPTS -->
    <script src="<c:url value="/resources/lib/js/custom.js"/>"></script>
    
   
</body>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@page session="true" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Admin Dashboard|Patient Information System</title>
</head>


<!-- Angular JS library -->
<script src="resources/lib/angularJS_library/angular.js"></script>
<script src="resources/lib/angularJS_library/angular.min.js"></script>


<!-- BOOTSTRAP STYLES-->
    <link href="resources/lib/css/bootstrap.css" rel="stylesheet" />
     <!-- FONTAWESOME STYLES-->
    <link href="resources/lib/css/font-awesome.css" rel="stylesheet" />
        <!-- CUSTOM STYLES-->
    <link href="resources/lib/css/custom.css" rel="stylesheet" />
     <!-- GOOGLE FONTS-->
   <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
   
   
   

<body ng-app="myApp" ng-controller="doctorController">
    <div id="wrapper">
         <div class="navbar navbar-inverse navbar-fixed-top">
            <div class="adjust-nav">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">
<!--                         <img src="assets/img/logo.png" /> -->
						
                    </a>
                    
                </div>
              
                <span class="logout-spn" >
              
                  <a href="logout" style="color:#fff;">LOGOUT</a>  

                </span>
            </div>
        </div>
        <!-- /. NAV TOP  -->
        <nav class="navbar-default navbar-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">
                 


                    <li class="active-link">
                        <a href="adminDashBoard" ><i class="fa fa-desktop "></i>Dashboard </a>
                    </li>
                   

                    <li>
                        <a href="addUser"><i class="fa fa-user "></i>Add User</a>
                    </li>
                    <li>
                        <a href="viewUsers"><i class="fa fa-users "></i>View User</a>
                    </li>


                    <li>
                        <a href="viewPatientForAdmin"><i class="fa fa-users "></i>View Patients</a>
                    </li>
                    <li>
                        <a href="viewDoctorForAdmin"><i class="fa fa-user-md"></i>View Doctors</a>
                    </li>

                    <li>
                        <a href="help"><i class="fa fa-envelope "></i>Help</a>
                    </li>
                    <li>
                        <a href="viewDepartmentForAdmin"><i class="fa fa-hospital-o "></i>Departments</a>
                    </li>
                   
                    
                </ul>
                            </div>

        </nav>
        <!-- /. NAV SIDE  -->
        <div id="page-wrapper" >
            <div id="page-inner">
                <div class="row">
                    <div class="col-lg-12">
					<h1>P & T Memorial Hospital</h1>
                     <h2>ADMIN DASHBOARD</h2> 
						
                    </div>
                </div>              
                 <!-- /. ROW  -->
                  <hr />
                <div class="row">
                    <div class="col-lg-12 ">
                        <div class="alert alert-info">
                             <strong>Welcome ${userInfo} ! </strong> Administrator panel
                        </div>
                       
                    </div>
                    </div>
                  
                 <!-- /. ROW  -->  
                 <div class="row text-center pad-top">
                             
                     <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
                      <div class="div-square">
                           <a href="addUser" >
 <i class="fa fa-user fa-5x"></i>
                      <h4>Register User</h4>
                      </a>
                      </div>
                     
                     
                  </div> 
                           
                     <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
                      <div class="div-square">
                           <a href="viewUsers" >
 <i class="fa fa-users fa-5x"></i>
                      <h4>View User</h4>
                      </a>
                      </div>
                     
                     
                  </div> 
                              
                     <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
                      <div class="div-square">
                           <a href="viewPatientForAdmin" >
 <i class="fa fa-users fa-5x"></i>
                      <h4>View Patient</h4>
                      </a>
                      </div>
                     
                     
                  </div> 
                  <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
                      <div class="div-square">
                           <a href="viewDoctorForAdmin" >
 <i class="fa fa-user-md fa-5x"></i>
                      <h4>View Doctors</h4>
                      </a>
                      </div>
                     
                     
                  </div>
                  <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
                      <div class="div-square">
                           <a href="viewDepartmentForAdmin" >
 <i class="fa fa-hospital-o fa-5x"></i>
                      <h4>View Departments </h4>
                      </a>
                      </div>
                     
                     
                  </div>
                  
                     <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
                      <div class="div-square">
                           <a href="help" >
 <i class="fa fa-envelope fa-5x"></i>
                      <h4>Help</h4>
                      </a>
                      </div>
                     
                     
                  </div> 
              </div>   
              
			  <div class="row">
                    <div class="col-lg-6 col-md-6">
                        <h5>OnDuty Doctors</h5>
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr>
                                    
                                    <th>First Name</th>
                                    <th>Last Name</th>
                                    <th>Department</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="doctor in availableDoctorList">
                                    
                                    <td>{{doctor.firstname}}</td>
                                    <td>{{doctor.lastname}}</td>
                                    <td>{{doctor.department.department_name}}</td>
                                </tr>
                               
                            </tbody>
                        </table>

                    </div>
			  
    </div>
	
	
	                    <div class="col-lg-6 col-md-6">
                        <h5>Off Duty Doctors</h5>
                       
                            <table class="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>First Name</th>
                                        <th>Last Name</th>
                                        <th>Department</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr ng-repeat="doctor in unavailableDoctorList">
                                       
                                        <td>{{doctor.firstname}}</td>
                                        <td>{{doctor.lastname}}</td>
                                        <td>{{doctor.department.department_name}}</td>
                                    </tr>
                                    
                                </tbody>
                            </table>
                       
                    </div>
             <!-- /. PAGE INNER  -->
            </div>
         <!-- /. PAGE WRAPPER  -->
        </div>
    <div class="footer">
      
    
            <div class="row">
                <div class="col-lg-12" >
                    &copy;  2016 P&T Memorial Hospital |  We care You
                </div>
            </div>
        </div>
          

    
     <!-- /. WRAPPER  -->
    <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
    <!-- JQUERY SCRIPTS -->
    <script src="resources/lib/js/jquery-1.10.2.js"></script>
      <!-- BOOTSTRAP SCRIPTS -->
    <script src="resources/lib/js/bootstrap.min.js"></script>
      <!-- CUSTOM SCRIPTS -->
    <script src="resources/lib/js/custom.js"></script>
   
</body>
<script>
var mainApp=angular.module('myApp',[]);
mainApp.controller('doctorController',function($scope,$http){
	
	$http.get('http://localhost:8080/PatientInformationSystem/availableDoctors').
		success(function(data,config,headers,status){
			$scope.availableDoctorList=data;
		}).error(function(config,headers,status){
			
		});
	
	$http.get('http://localhost:8080/PatientInformationSystem/unavailableDoctors').
		success(function(data,config,headers,status){
			$scope.unavailableDoctorList=data;
		}).error(function(){
			
		});
});

</script>

</html>
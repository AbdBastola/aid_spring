<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>    
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>  
<%@page session="true" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>View Patient Information|Patient Information System</title>
</head>

<%
String username=(String)session.getAttribute("user");
if(username==null){
	response.sendRedirect(request.getContextPath());
}
%>

<!-- BOOTSTRAP STYLES-->
    <link href="resources/lib/css/bootstrap.css" rel="stylesheet" />
     <!-- FONTAWESOME STYLES-->
    <link href="resources/lib/css/font-awesome.css" rel="stylesheet" />
        <!-- CUSTOM STYLES-->
    <link href="resources/lib/css/custom.css" rel="stylesheet" />
     <!-- GOOGLE FONTS-->
   <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
   
   
   

<body>
    <div id="wrapper">
         <div class="navbar navbar-inverse navbar-fixed-top">
            <div class="adjust-nav">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">
<!--                         <img src="assets/img/logo.png" /> -->
						
                    </a>
                    
                </div>
              
                <span class="logout-spn" >
              
                  <a href="logout" style="color:#fff;">LOGOUT</a>  

                </span>
            </div>
        </div>
        <!-- /. NAV TOP  -->
        <nav class="navbar-default navbar-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">
                 


                    <li class="active-link">
                        <a href="doctorDashBoard" ><i class="fa fa-desktop "></i>Dashboard </a>
                    </li>
                   

                    <li class="active-link">
                   	<a href="viewPatientInformation"><i class="fa fa-users"></i>View Patients Info</a>
                   </li>
                   
                   <li class="active-link">
                   	<a href="helpDoctor"><i class="fa fa-envelope"></i>Help</a>
                   </li>
                    
                </ul>
                            </div>

        </nav>
        <!-- /. NAV SIDE  -->
        <div id="page-wrapper" >
            <div id="page-inner">
                <div class="row">
                    <div class="col-lg-12">
					<h1>P & T Memorial Hospital</h1>
                     <h2>Doctor DASHBOARD</h2> 
						
                    </div>
                </div>              
                 <!-- /. ROW  -->
                  <hr />
                <div class="row">
                    <div class="col-lg-12 ">
                        <div class="alert alert-info">
                             <strong>Welcome <%=session.getAttribute("user") %> ! </strong> Doctor panel
                        </div>
                       
                       
                      <table class="table table-hover table-striped table-bordered">
                      <thead>
                      <tr>
                      <td>Patient Name</td>
                      <td>Age</td>
                      <td>Weight</td>
                      <td>Height</td>
                      <td>Temperature</td>
                      <td>Pulse</td>
                      <td>Blood Pressure</td>
                      <td>Chief Complaints</td>
                      <td>Provisional Diagnosis</td>
                      <td>Clinical Findings</td>
                      </tr>
                      </thead>
                      
                      <tbody>
                      <c:forEach var="patientInformation" items="${patientInformationList}">
                      <tr>
                      <td>${patientInformation.patient.name}</td>
                      <td>${patientInformation.patient.age}</td>
                      <td>${patientInformation.weight}</td>
                      <td>${patientInformation.height}</td>
                      <td>${patientInformation.temperature}</td>
                      <td>${patientInformation.pulse}</td>
                      <td>${patientInformation.bloodPressure}</td>
                      <td>${patientInformation.chiefComplaints}</td>
                      <td>${patientInformation.provisionalDiagnosis}</td>
                      <td>${patientInformation.clinicalFindings}</td>
                      </tr>
                      </c:forEach>
                      </tbody>
                      </table>
                       
                    </div>
                    </div>
                  
                            <!-- /. PAGE INNER  -->
            </div>
         <!-- /. PAGE WRAPPER  -->
        </div>
    <div class="footer">
      
    
            <div class="row">
                <div class="col-lg-12" >
                    &copy;  2016 P&T Memorial Hospital |  We care You
                </div>
            </div>
        </div>
          

    
     <!-- /. WRAPPER  -->
    <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
    <!-- JQUERY SCRIPTS -->
    <script src="resources/lib/js/jquery-1.10.2.js"></script>
      <!-- BOOTSTRAP SCRIPTS -->
    <script src="resources/lib/js/bootstrap.min.js"></script>
      <!-- CUSTOM SCRIPTS -->
    <script src="resources/lib/js/custom.js"></script>
   
</body>
</html>
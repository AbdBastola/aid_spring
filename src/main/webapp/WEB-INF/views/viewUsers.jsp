<%@page session="true" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>View User|Patient Information System</title>
</head>


<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>    
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>  


<%
String username=(String)session.getAttribute("user");
out.println("Hello");
if(username==null){
	response.sendRedirect(request.getContextPath());
}
%>



<!-- BOOTSTRAP STYLES-->
    <link href="resources/lib/css/bootstrap.css" rel="stylesheet" />
     <!-- FONTAWESOME STYLES-->
    <link href="resources/lib/css/font-awesome.css" rel="stylesheet" />
        <!-- CUSTOM STYLES-->
    <link href="resources/lib/css/custom.css" rel="stylesheet" />
     <!-- GOOGLE FONTS-->
   <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />


<body>

<!-- <a href="#addPatient">Add New Patient</a>

<div ng-view></div> -->

 <div id="wrapper">
         <div class="navbar navbar-inverse navbar-fixed-top">
            <div class="adjust-nav">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">
<!--                         <img src="assets/img/logo.png" /> -->
						
                    </a>
                    
                </div>
              
                <span class="logout-spn" >
                  <a href="logout" style="color:#fff;">LOGOUT</a>  

                </span>
            </div>
        </div>
        <!-- /. NAV TOP  -->
        <nav class="navbar-default navbar-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">
                 


                    <li class="active-link">
                        <a href="adminDashBoard" ><i class="fa fa-desktop "></i>Dashboard </a>
                    </li>
                   

                    <li>
                        <a href="addUser"><i class="fa fa-user "></i>Add User</a>
                    </li>
                    <li>
                        <a href="viewUsers"><i class="fa fa-users "></i>View User</a>
                    </li>


                    <li>
                        <a href="viewPatientForAdmin"><i class="fa fa-users "></i>View Patients</a>
                    </li>
                    <li>
                        <a href="viewDoctorForAdmin"><i class="fa fa-user-md"></i>View Doctors</a>
                    </li>

                    <li>
                        <a href="help"><i class="fa fa-envelope "></i>Help</a>
                    </li>
                    <li>
                        <a href="viewDepartmentForAdmin"><i class="fa fa-hospital-o "></i>Departments</a>
                    </li>
                   
                    
                </ul>
                            </div>

        </nav>
        <!-- /. NAV SIDE  -->
        
        <div id="page-wrapper" >
            <div id="page-inner">
                <div class="row">
                    <div class="col-lg-12">
					<h1>P & T Memorial Hospital</h1>
                     <h2>List of Users</h2> 
						
                    </div>
                </div>              
                 <!-- /. ROW  -->
                  <hr />
                <div class="row">
                    <div class="col-lg-12">
                        <div class="alert alert-info">
                             <strong>Welcome <%= session.getAttribute("user") %> ! </strong> You can now view all the users
                        </div>
                       
                       
          <table class="table table-striped table-bordered table-hover">
            <thead>
              <tr>
                <td>Id</td>
                <td>First name</td>
                <td>last name</td>
                <td>Address</td>
                <td>Username</td>
                <td>Password</td>
                <td>Roles</td>
                <td>Action</td>
              </tr>
            </thead>
              
              <tbody>
              <c:forEach items="${usersList}" var="user">
             <tr>
              	<td>${user.user_id}</td>
              	<td>${user.firstname}</td>
              	<td>${user.lastname}</td>
              	<td>${user.address}</td>
              	<td>${user.username}</td>
              	<td>${user.password}</td>
              	<td>
              	<c:forEach var="role" items="${user.roles}">
              	${role.role_name}
              	</c:forEach>
              	</td>
              	
              	
              	<td><a class="btn btn-success" href="editUser/${user.user_id}"><span class="glyphicon glyphicon-pencil"></span></a>|<a class="btn btn-danger" disabled href="deleteUser/${user.user_id}"><span class="glyphicon glyphicon-trash"></span></a></td>
             </tr>
              </c:forEach>
              </tbody>
 
          </table>
                       
                       
                    </div>
                    </div>
                  
               
                
               
              </div>   
         <!-- /. PAGE INNER  -->
            </div>
         <!-- /. PAGE WRAPPER  -->
        </div>
    <div class="footer">
      
    
            <div class="row">
                <div class="col-lg-12" >
                    &copy;  2016 P&T Memorial Hospital |  We care You
                </div>
            </div>
        </div>
          

    
     <!-- /. WRAPPER  -->
    <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
    <!-- JQUERY SCRIPTS -->
    <script src="resources/lib/js/jquery-1.10.2.js"></script>
      <!-- BOOTSTRAP SCRIPTS -->
    <script src="resources/lib/js/bootstrap.min.js"></script>
      <!-- CUSTOM SCRIPTS -->
    <script src="resources/lib/js/custom.js"></script>
   
              
</body>
</html>


<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>    
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 

<html ng-app="myApp">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Staffs Dashboard|Patient Information System</title>
</head>


	<%
	String username=(String)session.getAttribute("user");
	if(username==null){
		response.sendRedirect(request.getContextPath());
	}
	%>

<!-- BOOTSTRAP STYLES-->
    <link href="resources/lib/css/bootstrap.css" rel="stylesheet" />
     <!-- FONTAWESOME STYLES-->
    <link href="resources/lib/css/font-awesome.css" rel="stylesheet" />
        <!-- CUSTOM STYLES-->
    <link href="resources/lib/css/custom.css" rel="stylesheet" />
     <!-- GOOGLE FONTS-->
   <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
   
   <!-- jQuery library -->
   <script src="<c:url value="/resources/lib/jQuery/jquery-3.1.1.min.js"/>"></script>
   
   <!-- angular library -->
    <script src="<c:url value="/resources/lib/angularJS_library/angular.js" />"></script>
    <script src="<c:url value="/resources/lib/angularJS_library/angular.min.js" />"></script>
    <script src="<c:url value="/resources/lib/angularJS_library/angular-resource.js"/>"></script>
    <script src="<c:url value="/resources/lib/angularJS_library/angular-messages.js" />"></script>
    <script src="<c:url value="/resources/lib/angularJS_library/angular-messages.min.js"/>"></script>
   
   <!-- Angular code include in jsp -->
   <script src="<c:url value="/resources/angularjs/beds.js"/>" ></script>
   

<body ng-controller="bedController">
    <div id="wrapper">
         <div class="navbar navbar-inverse navbar-fixed-top">
            <div class="adjust-nav">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">
<!--                         <img src="assets/img/logo.png" /> -->
						
                    </a>
                    
                </div>
              
                <span class="logout-spn" >
              
                  <a href="logout" style="color:#fff;">LOGOUT</a>  

                </span>
            </div>
        </div>
        <!-- /. NAV TOP  -->
        <nav class="navbar-default navbar-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">
                 


                    <li class="active-link">
                        <a href="frontdeskstaffsDashBoard" ><i class="fa fa-desktop "></i>Staff Dashboard </a>
                    </li>
                   

                    <li>
                        <a href="addPatients"><i class="fa fa-user "></i>Add Patients</a>
                    </li>
                    <li>
                        <a href="viewPatients"><i class="fa fa-users "></i>View Patients</a>
                    </li>


                    <li>
                        <a href="addDoctors"><i class="fa fa-user-md "></i>Add Doctors</a>
                    </li>
                    <li>
                        <a href="viewDoctors"><i class="fa fa-user-md"></i>View Doctors</a>
                    </li>
					 <li>
                        <a href="viewBeds"><i class="fa fa-beds"></i>View Beds</a>
                    </li>

                    <li>
                        <a href="searchPatientForBedAssign"><i class="fa fa-beds"></i>Assign Beds</a>
                    </li>
                    <li>
                        <a href="departments"><i class="fa fa-hospital-o "></i>Departments</a>
                    </li>
                   
                    <li>
                        <a href="helpStaff"><i class="fa fa-envelope "></i>Help</a>
                    </li>
                   
                </ul>
                            </div>

        </nav>
        <!-- /. NAV SIDE  -->
        <div id="page-wrapper" >
            <div id="page-inner">
                <div class="row">
                    <div class="col-lg-12">
					<h1>P & T Memorial Hospital</h1>
                     <h2>Staff DASHBOARD</h2> 
						
                    </div>
                </div>              
                 <!-- /. ROW  -->
                  <hr />
                <div class="row">
                    <div class="col-lg-offset-3 col-lg-6">
                        <div class="alert alert-info">
                             <strong>Welcome <%=session.getAttribute("user") %> </strong>You can now assign or cancel the bed to patients- Staff panel
                        </div>
                       
                      <button id="assignBtn" class="btn btn-success">AssignBed</button>
                      <button id="cancelBtn" class="btn btn-success">CancelBed</button>
                      
                      <form:form method="POST" action="assignBed-assign" name="bedForm" commandName="bedInfo" modelAttribute="bedInfo" id="form1">
                       <h2>Patient Description</h2>
                       <input type="hidden" value="${patientInfo.getPatient_id()}" name="patientId">
                       <label class="control-label">Patient Name</label><input name="patientname" value="${patientInfo.getName()}" disabled="disabled" class="form-control">
                       <label class="control-label">Age/Sex</label><input name="patientgenderAge" value="${patientInfo.getAge()}/${patientInfo.getGender().getGender_name()}" disabled="disabled" class="form-control">
                       <hr/>
                      
                      <h2>Assign Bed</h2>
                      <label class="control-label">Please select the bed number you want assign</label>
                      <div class="controls" ng-init="getFreeBeds()">
                      <select name="freeBed" ng-model="freeBedDatas.model" name="freeBed" class="form-control" required="required">
                      <option value="">--Select Bed--</option>
                      <option ng-repeat="bed in freeBedDatas" value={{bed.bed_no}}>{{bed.bed_no}}|{{bed.bedType}}</option>
                      </select>
                      <div ng-messages="bedForm.freeBed.$error" ng-if="bedForm.freeBed.$dirty">
                      	<div ng-message="required">Please select the bed</div>
                      </div>
                      </div>
                      
                      <br>
                      <button type="submit" class="btn btn-primary" ng-disabled="bedForm.$invalid">Assign Bed</button>
                      </form:form>
                       
                       <form:form method="POST" action="cancelBed-cancel" name="cancelForm" commandName="bedInfo" modelAttribute="bedInfo" id="form2">
                       <h2>Patient Description</h2>
                       <input type="hidden" value="${patientInfo.getPatient_id()}" name="patientId">
                       <label class="control-label">Patient Name</label><input name="patientname" value="${patientInfo.getName()}" disabled="disabled" class="form-control">
                       <label class="control-label">Age/Sex</label><input name="patientgenderAge" value="${patientInfo.getAge()}/${patientInfo.getGender().getGender_name()}" disabled="disabled" class="form-control">
                       <hr/>
                      
                      <h2>Cancel Bed</h2>
                      <label class="control-label">Please select the bed number you want assign</label>
                      <div class="controls" ng-init="getOccupiedBeds()">
                      <select name="occupiedBed" ng-model="occupiedBedDatas.model" name="occupiedBed" class="form-control" required="required">
                      <option value="">--Select Bed--</option>
                      <option ng-repeat="bed in occupiedBedDatas" value={{bed.bed_no}}>{{bed.bed_no}}|{{bed.bedType}}</option>
                      </select>
                       <div ng-messages="cancelForm.occupiedBed.$error" ng-if="cancelForm.occupiedBed.$dirty">
                       	<div ng-message="required">Please select the bed</div>
                       </div>
                      </div>
                      
                      <br>
                      <button type="submit" class="btn btn-primary" ng-disabled="cancelForm.$invalid">Cancel Bed</button>
                      </form:form>
                       
                       
                       
                    </div>
                    </div>
                  
                  </div>
                   <div class="footer">
      
    
            <div class="row">
                <div class="col-lg-12" >
                    &copy;  2016 P&T Memorial Hospital |  We care You
                </div>
            </div>
        </div>
          

    
     <!-- /. WRAPPER  -->
    <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
    <!-- JQUERY SCRIPTS -->
    <script src="resources/lib/js/jquery-1.10.2.js"></script>
      <!-- BOOTSTRAP SCRIPTS -->
    <script src="resources/lib/js/bootstrap.min.js"></script>
      <!-- CUSTOM SCRIPTS -->
    <script src="resources/lib/js/custom.js"></script>
   
   <style>
   #form1{
   	display:none;
   }
   
   #form2{
   	display:none;
   }
   
   </style>
   
   <script>
   $(document).ready(function(){
	  $('#assignBtn').click(function(){
		  $('#form1').toggle('slow');
		  $('#form2').hide('hide');
	  }) ;
	  
	  $('#cancelBtn').click(function(){
		  $('#form2').toggle('slow');
		  $('#form1').hide('hide');
	  });
	   
   });
   
   
   </script>
   
</body>
</html>
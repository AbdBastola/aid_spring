<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>    
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>  
<%@page session="true" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>View Patient Information|Patient Information System</title>
</head>

<%
String username=(String)session.getAttribute("user");
if(username==null){
	response.sendRedirect(request.getContextPath());
}
%>

<!-- BOOTSTRAP STYLES-->
    <link href="resources/lib/css/bootstrap.css" rel="stylesheet" />
     <!-- FONTAWESOME STYLES-->
    <link href="resources/lib/css/font-awesome.css" rel="stylesheet" />
        <!-- CUSTOM STYLES-->
    <link href="resources/lib/css/custom.css" rel="stylesheet" />
     <!-- GOOGLE FONTS-->
   <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
   
   
   

<body>
    <div id="wrapper">
         <div class="navbar navbar-inverse navbar-fixed-top">
            <div class="adjust-nav">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">
<!--                         <img src="assets/img/logo.png" /> -->
						
                    </a>
                    
                </div>
              
                <span class="logout-spn" >
              
                  <a href="logout" style="color:#fff;">LOGOUT</a>  

                </span>
            </div>
        </div>
        <!-- /. NAV TOP  -->
        <nav class="navbar-default navbar-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">
                 


                    <li class="active-link">
                        <a href="doctorDashBoard" ><i class="fa fa-desktop "></i>Dashboard </a>
                    </li>
                   

                    <li class="active-link">
                   	<a href="viewPatientInformation"><i class="fa fa-users"></i>View Patients Info</a>
                   </li>
                   
                     <li class="active-link">
                   	<a href="helpDoctor"><i class="fa fa-envelope"></i>Help</a>
                   </li>
                    
                </ul>
                            </div>

        </nav>
        <!-- /. NAV SIDE  -->
        <div id="page-wrapper" >
            <div id="page-inner">
                <div class="row">
                    <div class="col-lg-12">
					<h1>P & T Memorial Hospital</h1>
                     <h2>Doctor DASHBOARD</h2> 
						
                    </div>
                </div>              
                 <!-- /. ROW  -->
                  <hr />
                <div class="row">
                    <div class="col-lg-12 ">
                        <div class="alert alert-info">
                             <strong>Welcome <%=session.getAttribute("user") %> ! </strong>Help Page-Doctor panel
                        </div>
                       
                       <h3>How to login?</h3>
                       <p>Start the system. You will see the login page with username and password field. Enter the valid username and password to the form. If the login credentials are correct you will be redirected to dashboard otherwise you will be asked to login again by the system</p>
                       
                       <h3>How to search patient?</h3>
                       <p>After you are logged into the system you will see the dashboard home page where there is a search field in it. Search the patient by their id to examine. After clicking the search button you will be redirected to add patient information page where patient info will be loaded and there are many fields where doctor have to fill after examining the patients</p>
                       
                       <h3>How to add patient information?</h3>
                       <p>After searching the patient add information form will appear, enter all the information of the patient after examining patient and click the add information button where you will be redirected to view patient information page</p>
                       
                       <h3>How to view patient information?</h3>
                       <p>To view patient information, click the view patient information link where all the patient information will be loaded successfully</p>
                     
                       <h3>How to logout from the system?</h3>
                       <p>In every page form dashboard to other there is logout link at the top right corner of the page.Click that link then you will be logout from the system</p>
                       
                    </div>
                    </div>
                  
                            <!-- /. PAGE INNER  -->
            </div>
         <!-- /. PAGE WRAPPER  -->
        </div>
    <div class="footer">
      
    
            <div class="row">
                <div class="col-lg-12" >
                    &copy;  2016 P&T Memorial Hospital |  We care You
                </div>
            </div>
        </div>
          

    
     <!-- /. WRAPPER  -->
    <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
    <!-- JQUERY SCRIPTS -->
    <script src="resources/lib/js/jquery-1.10.2.js"></script>
      <!-- BOOTSTRAP SCRIPTS -->
    <script src="resources/lib/js/bootstrap.min.js"></script>
      <!-- CUSTOM SCRIPTS -->
    <script src="resources/lib/js/custom.js"></script>
   
</body>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>    
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>  

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Update Doctor|Patient Information System</title>
</head>

<%
String username=(String)session.getAttribute("user");
if(username==null){
	response.sendRedirect(request.getContextPath());
}
%>

<!-- BOOTSTRAP STYLES-->

    <link href="<c:url value="/resources/lib/css/bootstrap.css" />" rel="stylesheet" />
     <!-- FONTAWESOME STYLES-->
     
    <link href="<c:url value="/resources/lib/css/font-awesome.css" />" rel="stylesheet" />
        <!-- CUSTOM STYLES-->
    
    <link href="<c:url value="/resources/lib/css/custom.css" />" rel="stylesheet" />
     <!-- GOOGLE FONTS-->
   <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
   
   
   

<body>
    <div id="wrapper">
         <div class="navbar navbar-inverse navbar-fixed-top">
            <div class="adjust-nav">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">
<!--                         <img src="assets/img/logo.png" /> -->
						
                    </a>
                    
                </div>
              
                <span class="logout-spn" >
              
                  <a href="logout" style="color:#fff;">LOGOUT</a>  

                </span>
            </div>
        </div>
        <!-- /. NAV TOP  -->
        <nav class="navbar-default navbar-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">
                 


                    <li class="active-link">
                        <a href="${pageContext.request.contextPath}/frontdeskstaffsDashBoard" ><i class="fa fa-desktop "></i>Staff Dashboard </a>
                    </li>
                   

                    <li>
                        <a href="${pageContext.request.contextPath}/addPatients"><i class="fa fa-user "></i>Add Patients</a>
                    </li>
                    <li>
                        <a href="${pageContext.request.contextPath}/viewPatients"><i class="fa fa-users "></i>View Patients</a>
                    </li>


                    <li>
                        <a href="${pageContext.request.contextPath}/addDocotrs"><i class="fa fa-user-md "></i>Add Doctors</a>
                    </li>
                    <li>
                        <a href="${pageContext.request.contextPath}/viewDoctors"><i class="fa fa-user-md"></i>View Doctors</a>
                    </li>

                    <li>
                        <a href="${pageContext.request.contextPath}/assignBed"><i class="fa fa-bed "></i>Assign Beds</a>
                    </li>
                    <li>
                        <a href="${pageContext.request.contextPath}/departments"><i class="fa fa-hospital-o "></i>Departments</a>
                    </li>
                   
                    <li>
                        <a href="${pageContext.request.contextPath}/helpStaff"><i class="fa fa-envelope "></i>Help</a>
                    </li>
                   
                </ul>
                            </div>

        </nav>
        <!-- /. NAV SIDE  -->
        <div id="page-wrapper" >
            <div id="page-inner">
                <div class="row">
                    <div class="col-lg-12">
					<h1>P & T Memorial Hospital</h1>
                     <h2>Staff DASHBOARD</h2> 
						
                    </div>
                </div>              
                 <!-- /. ROW  -->
                  <hr />
                <div class="row">
                    <div class="col-lg-offset-3 col-lg-6 ">
                        <div class="alert alert-info">
                             <strong>Welcome ${userInfo} ! </strong> Staff panel
                        </div>
                       
                      
                       
                   <form:form action="${pageContext.request.contextPath}/updateDoctor-update" method="POST" modelAttribute="doctorInfo" commandName="doctorInfo" class="form-horizontal">
                   		
                   		<form:hidden path="doctor_id" />
                   		
                   		<label class="control-label" for="firstname">Firstname</label>
                   		<div class="controls">
                   			<form:input path="firstname" class="form-control" id="firstname" required="required"/>
                   			 <p class="help-block">Firstname can contain any letters, without spaces</p>
                   		</div>
                   		
                   		<label class="control-label" for="lastname">Lastname</label>
                   		<div class="controls">
                   			<form:input path="lastname" class="form-control" id="lastname" required="required"/>
                   			 <p class="help-block">Lastname can contain any letters, without spaces</p>
                   		</div>
                   		
                   		<label class="control-label" for="specialization">Specialization</label>
                   		<div class="controls">
                   			<form:input path="specialization" class="form-control" id="specialization" required="required"/>
                   			 <p class="help-block">Specialization can contain any letters or numbers, without spaces</p>
                   		</div>
                   		
                   		<label class="control-label" for="degree">Degree</label>
                   		<div class="controls">
                   			<form:input path="degree" class="form-control" id="degree" required="required"/>
                   			 <p class="help-block">Degree can contain any letters, without spaces</p>
                   		</div>
                   		
                   		<label class="control-label" for="arrivalTime">Arrival Time</label>
                   		<div class="controls">
                   			<form:input path="arrivalTime" class="form-control" id="arrivalTime" required="required"/>
                   			 <p class="help-block">ArrivalTime can contain only numbers in the format of 12:00</p>
                   		</div>
                   		
                   		<label class="control-label" for="departureTime">Departure Time</label>
                   		<div class="controls">
                   			<form:input path="departureTime" class="form-control" id="departureTime" required="required"/>
                   			<p class="help-block">DepartureTime can contain only numbers in the format of 12:00</p>
                   		</div>
                   		
                   		<label class="control-label">Select Department</label>
                  		<div class="controls">
                    		<form:select path="department" items="${departmentList}" itemLabel="department_name" itemValue="dept_id" class="form-control"/>
                  		</div>
                   		
                   		<br/>
                 <button type="submit" class="btn btn-success">Update Doctor </button>
                   		
                   		
                   </form:form>   
                       
                    </div>
                    
                    
                    </div>
                  
                 <!-- /. ROW  -->  
                 
              
			 
	
	
             <!-- /. PAGE INNER  -->
            </div>
         <!-- /. PAGE WRAPPER  -->
        </div>
    <div class="footer">
      
    
            <div class="row">
                <div class="col-lg-12" >
                    &copy;  2016 P&T Memorial Hospital |  We care You
                </div>
            </div>
        </div>
          

    
     <!-- /. WRAPPER  -->
    <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
    <!-- JQUERY SCRIPTS -->
    
    <script src="<c:url value="/resources/lib/js/jquery-1.10.2.js"/>"></script>
      <!-- BOOTSTRAP SCRIPTS -->
      
    <script src="<c:url value="/resources/lib/js/bootstrap.min.js"/>"></script>
      <!-- CUSTOM SCRIPTS -->
      
    <script src="<c:url value="/resources/lib/js/custom.js"/>"></script>
   
</body>
</html>
package com.soft.patientinformationsystem.Controller;

import java.beans.PropertyEditorSupport;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.WebRequest;

import com.soft.patientinformationsystem.Service.DepartmentService;
import com.soft.patientinformationsystem.Service.DoctorService;
import com.soft.patientinformationsystem.Service.PatientService;
import com.soft.patientinformationsystem.Service.RoleService;
import com.soft.patientinformationsystem.Service.UserService;
import com.soft.patientinformationsystem.model.User;

@Controller //annotation that defines that UserController class is a controller in MVC pattern of spring framework
public class UserController {

	@Autowired private UserService userService;  //autowiring userService using DI provided by spring framework
	
	@Autowired private RoleService roleService;  //autowiring roleService
	
	@Autowired private DoctorService doctorService;
	
	@Autowired private PatientService patientService;
	
	@Autowired private DepartmentService deptService;
	
	private static final Logger logger=Logger.getLogger(UserController.class);  //initializing log4j to track the log of this controller class

	
	//login and authentication part begins here
	
	@RequestMapping(value="/",method=RequestMethod.GET)
	public String executeLogin(@RequestParam(value="error",required=false)String error,
			@RequestParam(value="logout",required=false)String logout,Model model,@ModelAttribute("userInfo")User user){
	
		
		if(error!=null){
			model.addAttribute("error", "Username and password cannot be null");
		}
		else if(logout!=null){
			model.addAttribute("logout", "You have been logout out successfully");
		}
		return "login";
	}
	
	
	public String getPrincipal(){
		String username=null;
		Object principal=SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		
		if(principal instanceof UserDetails){
			username=((UserDetails) principal).getUsername();
			
		}else{
			
			username=null;
		}
		return username;
	}
	
	
	@RequestMapping(value="/admin**",method=RequestMethod.GET)
	public String loadAdminPage(Model model){
		model.addAttribute("userInfo", getPrincipal());
		return "adminDashBoard";
	}
	
	
	
	@RequestMapping(value="/frontdeskstaffs**",method=RequestMethod.GET)
	public String loadFrontDeskStaffPage(Model model){
		model.addAttribute("userInfo", getPrincipal());
		return "frontdeskstaffDashBoard";
	}
	

	@RequestMapping(value="/doctor**",method=RequestMethod.GET)
	public String loaddoctorPage(Model model){
		model.addAttribute("userInfo", getPrincipal());
		return "doctorDashBoard";
	}
	

	@RequestMapping(value="/labtechnician**",method=RequestMethod.GET)
	public String loadlabTechnicianPage(Model model){
		model.addAttribute("userInfo", getPrincipal());
		return "labtechnicianDashBoard";
	}
	
	@RequestMapping(value="/403",method=RequestMethod.GET)
	public String accessDeniedForUser(){
		Authentication auth=SecurityContextHolder.getContext().getAuthentication();
		String username="";
		if(!(auth instanceof AnonymousAuthenticationToken)){
			UserDetails userDetails=(UserDetails)auth.getPrincipal();
			username=userDetails.getUsername();
		}
		return "403";
	}
	
	
	@RequestMapping(value="/logout",method=RequestMethod.GET)
	public String logoutPage(HttpServletRequest request,HttpServletResponse response){
		Authentication auth=SecurityContextHolder.getContext().getAuthentication();
		if(auth!=null){
			new SecurityContextLogoutHandler().logout(request, response, auth);
		}
		 return "redirect:/?logout";
	}
	
	//validating 
	@InitBinder
	public void initBinder(WebDataBinder binder,WebRequest request){
		binder.registerCustomEditor(User.class,"roles", new PropertyEditorSupport(){
			public void setAsText(String text){
				System.out.println(text);
				setValue((text.equals(""))?null:roleService.getRoleById(Integer.parseInt(text)));
			}
		});
		
	}
	
	
	
	//http://localhost:8080/PatientInformationSystem/addUser will be mapped to this method
	@RequestMapping(value="/addUser",method=RequestMethod.GET)
	public String loadAddUserPage(Model model,HttpSession session){
		model.addAttribute("userInfo", new User());//sending user object to jsp that binds all the properties of user class
		model.addAttribute("roleList", roleService.getAllRoles());  //sending all the role list to jsp 
		session.setAttribute("user", getPrincipal());
		return "addUsers";  //returning the name of jsp that will be displayed in browser
	}
	
	//this method will be invoked when user is to registered to the system and method will be post
	@RequestMapping(value="/addUser-add",method=RequestMethod.POST)
	public String executeAddUserPage(@RequestParam(value="roles")Integer role_id,@ModelAttribute("userInfo")User user,BindingResult result){
		
		if(result.hasErrors()){
			System.out.println("Error caught out using Binding Result");
		}
		
		if(logger.isDebugEnabled()){   //log4j
			logger.debug("executeAddUserPage is executed");
		}
		
		logger.error("This is Error message", new Exception("Testing"));  //error tracking log4j
		
		System.out.println(role_id);
		userService.saveUser(user, role_id);
		
		return "redirect:/viewUsers"; //redirecting the page to view users where all the users will be displayed
	}
	
	
	//http://localhost:8080/PatientInformationSystem/viewUsers will be mapped to this method
	@RequestMapping(value="/viewUsers",method=RequestMethod.GET)
	public String loadViewUsersPage(Model model,HttpSession session){
		model.addAttribute("usersList", userService.getAllUsers());  //sending all the users as a list to jsp
		session.setAttribute("user", getPrincipal());
		return "viewUsers"; //view name
	}
	
	//http://localhost:8080/PatientInformationSystem/editUser/12  will be mapped to this method
	@RequestMapping(value="/editUser/{user_id}",method=RequestMethod.GET)
	public String loadUpdatePage(@PathVariable("user_id") Integer user_id,Model model,HttpSession session){  //Path Variable will be used to consume the id form the given url
		model.addAttribute("userInfo", userService.getUserById(user_id));  //sending the user info to jsp according to user_id
		model.addAttribute("roleList", roleService.getAllRoles());
		session.setAttribute("user", getPrincipal());
		return "updateUser";  //view name
	}
	
	
	@RequestMapping(value="/updateUser-update",method=RequestMethod.POST)
	public String executeUpdatePage(@RequestParam("roles")Integer role_id,@ModelAttribute("userInfo")User user,BindingResult result){
		if(result.hasErrors()){
			
		}
		
		userService.updateUser(user, role_id);
		return "redirect:/viewUsers";
	}
	
	//http://localhost:8080/PatientInformationSystem/deleteUser/12 will be mapped to this method
	@RequestMapping(value="/deleteUser/{user_id}",method=RequestMethod.GET)
	public String executeDeletePage(@PathVariable("user_id")Integer user_id){
		userService.deleteUser(user_id); //calling userService class method to delete a user
		return "redirect:/viewUsers";  //redirecting to view name
	}
	
	@RequestMapping(value="/viewDoctorForAdmin",method=RequestMethod.GET)
	public String viewDoctorForAdmin(Model model,HttpSession session){
		model.addAttribute("doctorsList", doctorService.getAllDoctors());
		session.setAttribute("user", getPrincipal());
		return "viewDoctorForAdmin";
	}
	
	@RequestMapping(value="/viewPatientForAdmin",method=RequestMethod.GET)
	public String viewPatientForAdmin(Model model,HttpSession session){
		model.addAttribute("patientList", patientService.getAllPatients());
		session.setAttribute("user", getPrincipal());
		return "viewPatientForAdmin";
	}
	
	@RequestMapping(value="/viewDepartmentForAdmin",method=RequestMethod.GET)
	public String viewDepartmentForAdmin(Model model,HttpSession session){
		model.addAttribute("departmentList", deptService.getAllDepartments());
		session.setAttribute("user", getPrincipal());
		return "viewDepartmentForAdmin";
	}
	
	@RequestMapping(value="/usernameExists",method=RequestMethod.GET)
	public @ResponseBody boolean isUsernameExists(@RequestParam("username")String username){
		return userService.isUsernameExists(username);
	}
	
	@RequestMapping(value="/help",method=RequestMethod.GET)
	public String loadHelpPage(HttpSession session){
		session.setAttribute("user", getPrincipal());
		return "helpPageforAdminUser";
	}
}

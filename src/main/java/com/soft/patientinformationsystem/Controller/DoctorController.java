package com.soft.patientinformationsystem.Controller;


import java.beans.PropertyEditorSupport;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.WebRequest;

import com.soft.patientinformationsystem.Service.DepartmentService;
import com.soft.patientinformationsystem.Service.DoctorService;
import com.soft.patientinformationsystem.model.Doctor;

@Controller
public class DoctorController {

	@Autowired private DoctorService doctorService;
	@Autowired private DepartmentService departmentService;
	@Autowired private UserController uc;
	
	
	@InitBinder
	public void initBinder(WebDataBinder binder,WebRequest request){
		binder.registerCustomEditor(Doctor.class,"department",new PropertyEditorSupport(){
			public void setAsText(String text){
				setValue((text.equals(""))?null:departmentService.getDepartmentById(Integer.parseInt(text)));
			}
		});
	}
	
	
	
	
	@RequestMapping(value="/addDoctors",method=RequestMethod.GET)
	public String loadAddDoctorsPage(Model model,HttpSession session){
		model.addAttribute("doctorInfo", new Doctor());
		model.addAttribute("departmentList", departmentService.getAllDepartments());
		session.setAttribute("user", uc.getPrincipal());
		return "addDoctors";	
	}
	
	@RequestMapping(value="/viewDoctors",method=RequestMethod.GET)
	public String loadViewDoctorsPage(Model model,HttpSession session){
		model.addAttribute("doctorsList",doctorService.getAllDoctors());
		session.setAttribute("user", uc.getPrincipal());
		return "viewDoctors";
	}
	
	@RequestMapping(value="/viewAllDoctors",method=RequestMethod.GET)
	public @ResponseBody List<Doctor> getAllDoctors(){
		return doctorService.getAllDoctors();
	}
	
	@RequestMapping(value="/scheduleDoctors",method=RequestMethod.GET)
	public String loadschedulePage(Model model,HttpSession session){
		model.addAttribute("doctorsList", doctorService.getAllDoctors());
		session.setAttribute("user", uc.getPrincipal());
		return "scheduleDoctors";
		
	}
	
	@RequestMapping(value="/isDoctorAvailable/{doctor_id}",method=RequestMethod.GET)
	public @ResponseBody void isDoctorAvailable(@PathVariable("doctor_id")Integer doctor_id){
		System.out.println(doctor_id);
		doctorService.isDoctorAvailable(doctor_id);
	}
	
	@RequestMapping(value="isDoctorOff/{doctor_id}",method=RequestMethod.GET)
	public @ResponseBody void isDoctorOff(@PathVariable("doctor_id")Integer doctor_id){
		doctorService.isDoctorOff(doctor_id);
	}
	
	@RequestMapping(value="/addDoctors-add",method=RequestMethod.POST)
	public String executeAddDoctorsPage(@RequestParam("department")Integer dept_id,@ModelAttribute("doctorInfo")Doctor doctor,BindingResult result){
		
		if(result.hasErrors()){
			System.out.println("Error caught out using Binding Result");
		}
		System.out.println(dept_id);
		doctorService.saveDoctor(doctor, dept_id);
		return "redirect:/viewDoctors";
	}
	
	@RequestMapping(value="/editDoctor/{doctor_id}",method=RequestMethod.GET)
	public String loadUpdatePage(@PathVariable("doctor_id")Integer doctor_id,Model model,HttpSession session){
		Doctor doctor=doctorService.getDoctorById(doctor_id);
		model.addAttribute("doctorInfo", doctor);
		model.addAttribute("departmentList", departmentService.getAllDepartments());
		session.setAttribute("user", uc.getPrincipal());
		return "updateDoctor";
	}
	
	@RequestMapping(value="/updateDoctor-update",method=RequestMethod.POST)
	public String executeUpdatePage(@RequestParam("department")Integer dept_id,@ModelAttribute("doctorInfo")Doctor doctor,BindingResult result){
		if(result.hasErrors()){
			
		}
		doctorService.updateDoctor(doctor, dept_id);
		return "redirect:/viewDoctors";
	}
	
	@RequestMapping(value="/deleteDoctor/{doctor_id}",method=RequestMethod.GET)
	public String deleteDoctors(@PathVariable("doctor_id")Integer doctor_id){
		doctorService.deleteDoctor(doctor_id);
		return "redirect:/viewDoctors";
	}
	
	@RequestMapping(value="/availableDoctors",method=RequestMethod.GET)
	public @ResponseBody List<Doctor> getAllAvailableDoctors(){
		return doctorService.getAllAvailableDoctors();
	}
	
	@RequestMapping(value="/unavailableDoctors",method=RequestMethod.GET)
	public @ResponseBody List<Doctor> getAllUnavailableDoctors(){
		return doctorService.getAllUnAvailableDoctors();
	}
	
	@RequestMapping(value="/helpDoctor",method=RequestMethod.GET)
	public String loadHelpPageForDoctor(HttpSession session){
		session.setAttribute("user", uc.getPrincipal());
		return "helpPageForDoctor";
	}
}

package com.soft.patientinformationsystem.Controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.soft.patientinformationsystem.Service.PatientInformationService;
import com.soft.patientinformationsystem.model.PatientInformation;

@Controller
public class PatientInformationController {

	@Autowired private PatientInformationService patientInformationSerivce;
	@Autowired private UserController uc;
	
	@RequestMapping(value="/viewPatientInformation",method=RequestMethod.GET)
	public String loadViewPatientInformationPage(Model model,HttpSession session){
		session.setAttribute("user", uc.getPrincipal());
		model.addAttribute("patientInformationList", patientInformationSerivce.getAllPatientInformation());
		return "viewPatientInformation";
	}
	
	@RequestMapping(value="/patientInfo-add",method=RequestMethod.POST)
	public String executePatientInfoPage(@RequestParam("patientId")String patient_id,@ModelAttribute("patientInformation")PatientInformation patientInfomration){
	
		System.out.println(patient_id);
		patientInformationSerivce.savePatientInformation(Integer.parseInt(patient_id),patientInfomration);
		return "success";
	}
}

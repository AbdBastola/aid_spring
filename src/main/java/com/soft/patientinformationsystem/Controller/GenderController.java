package com.soft.patientinformationsystem.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.soft.patientinformationsystem.Service.GenderService;
import com.soft.patientinformationsystem.model.Gender;

@RestController
public class GenderController {

	
	@Autowired private GenderService genderService;
	
	@RequestMapping(value="/getGenders",method=RequestMethod.GET)
	public List<Gender> getAllGenders(){
		return this.genderService.getAllGenders();
	}
}

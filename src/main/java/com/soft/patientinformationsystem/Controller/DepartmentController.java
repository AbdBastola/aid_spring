package com.soft.patientinformationsystem.Controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.soft.patientinformationsystem.Service.DepartmentService;
import com.soft.patientinformationsystem.model.Department;

@Controller
public class DepartmentController {

	@Autowired private DepartmentService departmentService;
	@Autowired private UserController uc;
	
	@RequestMapping(value="/departments",method=RequestMethod.GET)
	public String loadAllDepartments(Model model,HttpSession session){
		model.addAttribute("departmentList", departmentService.getAllDepartments());
		session.setAttribute("user", uc.getPrincipal());
		return "departments";
	}

	@RequestMapping(value="/getDepartments",method=RequestMethod.GET)
	public @ResponseBody List<Department> getAllDepartments(){
		System.out.println("--------------------------------------------------------");
		return departmentService.getAllDepartments();
	}
}

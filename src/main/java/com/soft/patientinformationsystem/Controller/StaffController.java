package com.soft.patientinformationsystem.Controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class StaffController {

	@Autowired UserController uc;
	
	@RequestMapping(value="/helpStaff",method=RequestMethod.GET)
	public String loadHelpPageForStaff(HttpSession session){
		session.setAttribute("user",uc.getPrincipal());
		return "helpPageForStaff";
	}

}

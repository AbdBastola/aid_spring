package com.soft.patientinformationsystem.Controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.soft.patientinformationsystem.Service.BedService;
import com.soft.patientinformationsystem.Service.PatientService;
import com.soft.patientinformationsystem.model.Bed;
import com.soft.patientinformationsystem.model.Patient;

@Controller
public class BedController {

	@Autowired private PatientService patientService;
	@Autowired private BedService bedService;
	@Autowired private UserController uc;
	
	@RequestMapping(value="/searchPatientForBedAssign",method=RequestMethod.GET)
	public String loadsearchPageForBedAssign(HttpSession session){
		session.setAttribute("user", uc.getPrincipal());
		return "searchPatientForBedAssign";
	}
	
	@RequestMapping(value="/viewBeds",method=RequestMethod.GET)
	public String loadViewBedsPage(HttpSession session){
		session.setAttribute("user", uc.getPrincipal());
		return "viewBeds";
	}
	
	@RequestMapping(value="/assignBed",method=RequestMethod.GET)
	public String assignBedForPatient(@RequestParam("searchId")String patient_id,Model model,HttpSession session){
		System.out.println(patient_id);
		session.setAttribute("user", uc.getPrincipal());
		Patient patient=patientService.getPatientById(Integer.parseInt(patient_id));
		model.addAttribute("patientInfo", patient);
		model.addAttribute("bedInfo", new Bed());
		return "assignBed";
	}
	
	@RequestMapping(value="/assignBed-assign",method=RequestMethod.POST)
	public String executeAssignBed(@RequestParam("freeBed")String bed_id,@RequestParam("patientId")String patient_id,@ModelAttribute("bedInfo")Bed bed){
		bedService.assignBed(Integer.parseInt(patient_id),Integer.parseInt(bed_id));
		return "viewBeds";
	}
	
	@RequestMapping(value="/cancelBed-cancel",method=RequestMethod.POST)
	public String executeCancelBed(@RequestParam("occupiedBed")String bed_id){
		bedService.cancelBed(Integer.parseInt(bed_id));
		return "viewBeds";
	}
	
	@RequestMapping(value="getAllICUBeds",method=RequestMethod.GET)
	public @ResponseBody List<Bed> getAllICUBeds(){
		return bedService.getICUBeds();
	}
	
	@RequestMapping(value="getAllGeneralWardBeds",method=RequestMethod.GET)
	public @ResponseBody List<Bed> getAllGeneralWardBeds(){
		return bedService.getGeneralWardBeds();
	}
	
	@RequestMapping(value="getAllMaternityWardBeds",method=RequestMethod.GET)
	public @ResponseBody List<Bed> getAllMaternityWardBeds(){
		return bedService.getMaternityWardBeds();
	}
	
	@RequestMapping(value="getAllFreeBeds",method=RequestMethod.GET)
	public @ResponseBody List<Bed> getAllFreeBeds(){
		return bedService.getFreebeds();
	}
	
	@RequestMapping(value="getOccupiedBeds",method=RequestMethod.GET)
	public @ResponseBody List<Bed> getOccupiedBeds(){
		return bedService.getOccupiedBeds();
	}
}

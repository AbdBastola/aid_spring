package com.soft.patientinformationsystem.Controller;


import java.beans.PropertyEditorSupport;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.WebRequest;

import com.soft.patientinformationsystem.Service.DepartmentService;
import com.soft.patientinformationsystem.Service.DoctorService;
import com.soft.patientinformationsystem.Service.GenderService;
import com.soft.patientinformationsystem.Service.PatientService;
import com.soft.patientinformationsystem.model.Doctor;
import com.soft.patientinformationsystem.model.Patient;
import com.soft.patientinformationsystem.model.PatientInformation;

@Controller
public class PatientController {

	//Autowired every field 
	@Autowired private PatientService patientService;
	@Autowired private GenderService genderService;
	@Autowired private DepartmentService departmentService;
	@Autowired private DoctorService doctorService;
	@Autowired private UserController uc;
	
	
	//binding the gender object from form and converting to respective id
	@InitBinder
	public void bindGender(WebDataBinder binder,WebRequest request){
		binder.registerCustomEditor(Patient.class, "gender",new PropertyEditorSupport(){
			@Override
			public void setAsText(String text){
				setValue((text.equals(""))?null:genderService.getGenderById(Integer.parseInt(text)));
			}
		});
	}
	
	@InitBinder
	public void bindDepartment(WebDataBinder binder,WebRequest request){
		binder.registerCustomEditor(Patient.class, "department",new PropertyEditorSupport(){
			@Override
			public void setAsText(String text) {
				// TODO Auto-generated method stub
				setValue((text.equals(""))?null:departmentService.getDepartmentById(Integer.parseInt(text)));
			}
		});
	}
	
	@InitBinder
	public void bindDoctor(WebDataBinder binder,WebRequest request){
		binder.registerCustomEditor(Patient.class, "doctor", new PropertyEditorSupport(){
			@Override
			public void setAsText(String text) throws IllegalArgumentException {
				// TODO Auto-generated method stub
				setValue((text.equals(""))?null:doctorService.getDoctorById(Integer.parseInt(text)));
			}
		});
	}
	
	@RequestMapping(value="/docByDepartment",method=RequestMethod.GET)
	public @ResponseBody List<Doctor> getDoctorByDepartment(@RequestParam("department")String dept_id){
		System.out.println(dept_id);
		return doctorService.getDoctorByDepartment(Integer.parseInt(dept_id));
	}
	
	@RequestMapping(value="/timepass",method=RequestMethod.GET)
	public String loadtimepass(){
		return "timepass";
	}
	
	@RequestMapping(value="/timepass-add",method=RequestMethod.GET)
	public String executeTimePass(@RequestParam("repeatSelect")String id){
		
		System.out.println(id);
		return "success";
	}
	
	@RequestMapping(value="/addPatients",method=RequestMethod.GET)
	public String loadAddPatientsPage(Model model,HttpSession session){
		model.addAttribute("patientInfo", new Patient());
		session.setAttribute("user", uc.getPrincipal());
		return "addPatients";
	}
	
	
	@RequestMapping(value="/addPatients-add",method=RequestMethod.GET)
	public String executeAddPatientsPage(@RequestParam("gender")String gender_id,@RequestParam("department")String dept_id,
			@RequestParam("doctor")String doctor_id,@ModelAttribute("patientInfo")Patient patient,
			BindingResult result){
		
		if(result.hasErrors()){
			
		}
		patientService.savePatient(patient, Integer.parseInt(gender_id), Integer.parseInt(dept_id), Integer.parseInt(doctor_id));
		return "redirect:/viewPatients";
	}
	
	@RequestMapping(value="/viewPatients",method=RequestMethod.GET)
	public String loadViewPatientPage(Model model,HttpSession session){
		model.addAttribute("patientList", patientService.getAllPatients());
		session.setAttribute("user", uc.getPrincipal());
		return "viewPatients";
		
	}
	
	@RequestMapping(value="/editPatient/{patient_id}",method=RequestMethod.GET)
	public String loadEditPatientPage(@PathVariable("patient_id")Integer patient_id,Model model,HttpSession session){
		model.addAttribute("patientInfo", patientService.getPatientById(patient_id));
		//model.addAttribute("genderList", genderService.getAllGenders());
		//model.addAttribute("doctorsList", doctorService.getAllDoctors());
		//model.addAttribute("departmentList", departmentService.getAllDepartments());
		session.setAttribute("user", uc.getPrincipal());
		return "updatePatient";
	}
	
	@RequestMapping(value="/updatePatient-update",method=RequestMethod.POST)
	public String executeEditPatientPage(@RequestParam(value="gender",required=false)String gender_id,
			@RequestParam(value="department",required=false)String dept_id,@RequestParam(value="doctor",required=false)String doctor_id,
			@ModelAttribute("patientInfo")Patient patient,BindingResult result){
		
		if(result.hasErrors()){
			System.out.println(result);
		}
	patientService.updatePatient(patient, Integer.parseInt(gender_id),Integer.parseInt(dept_id),Integer.parseInt(doctor_id));
		
		return "redirect:/viewPatients";
	}
	
	@RequestMapping(value="deletePatient/{patient_id}",method=RequestMethod.GET)
	public String loadDeletePatientPage(@PathVariable("patient_id")Integer patient_id){
		patientService.deletePatient(patient_id);
		return "redirect:/viewPatients";
	}
	
	@RequestMapping(value="/searchPatient",method=RequestMethod.GET)
	public String searchPatinet(@RequestParam("searchId")String patient_id,Model model,HttpSession session){
		System.out.println(patient_id);
		Patient patientInfo=patientService.getPatientById(Integer.parseInt(patient_id));
		model.addAttribute("patientInfo",patientInfo);
		model.addAttribute("patientInformation", new PatientInformation());
		session.setAttribute("user", uc.getPrincipal());
		return "patientInfo";
	}
	
}

package com.soft.patientinformationsystem.Service;

import java.util.List;

import com.soft.patientinformationsystem.model.Role;

public interface RoleService {

	public List<Role> getAllRoles();
	public Role getRoleById(Integer role_id);
}

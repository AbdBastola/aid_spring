package com.soft.patientinformationsystem.Service;

import com.soft.patientinformationsystem.model.PatientInformation;

import java.util.List;

public interface PatientInformationService {

	public void savePatientInformation(Integer patient_id,PatientInformation patientInformation);
	public List<PatientInformation> getAllPatientInformation();
}

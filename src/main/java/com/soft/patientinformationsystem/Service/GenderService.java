package com.soft.patientinformationsystem.Service;

import java.util.List;

import com.soft.patientinformationsystem.model.Gender;

public interface GenderService {

	public List<Gender> getAllGenders();
	public Gender getGenderById(Integer gender_id);
}

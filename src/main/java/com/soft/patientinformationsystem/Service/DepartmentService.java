package com.soft.patientinformationsystem.Service;

import java.util.List;

import com.soft.patientinformationsystem.model.Department;

public interface DepartmentService {

	public List<Department> getAllDepartments();
	public Department getDepartmentById(Integer id);
}

package com.soft.patientinformationsystem.Service;

import java.util.List;

import com.soft.patientinformationsystem.model.Patient;

public interface PatientService{

	public void savePatient(Patient patient,Integer gender_id,Integer dept_id,Integer doctor_id);
	public void updatePatient(Patient patient,Integer gender_id,Integer dept_id,Integer doctor_id);
	public List<Patient> getAllPatients();
	public Patient getPatientById(Integer patient_id);
	public void deletePatient(Integer patient_id);
}

package com.soft.patientinformationsystem.DAO;

import java.util.List;

import com.soft.patientinformationsystem.model.PatientInformation;

public interface PatientInformationDAO {

	public void savePatientInformation(Integer patient_id,PatientInformation patientInformation);
	public List<PatientInformation> getAllPatientInformation();
}

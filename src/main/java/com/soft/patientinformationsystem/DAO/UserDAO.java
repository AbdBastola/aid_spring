package com.soft.patientinformationsystem.DAO;

import java.util.List;

import com.soft.patientinformationsystem.model.User;

public interface UserDAO {

	public void saveUser(User user,Integer role_id);
	public void updateUser(User user,Integer role_id);
	public List<User> getAllUsers();
	public User getUserById(Integer id);
	public void deleteUser(Integer id);
	public User findByUsername(String username);
	public boolean isUsernameExists(String username);
}

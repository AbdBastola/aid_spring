package com.soft.patientinformationsystem.DAO;

import java.util.List;

import com.soft.patientinformationsystem.model.Doctor;

public interface DoctorDAO {

	public void saveDoctor(Doctor doctor,Integer dept_id);
	public void updateDoctor(Doctor doctor,Integer dept_id);
	public List<Doctor> getAllDoctors();
	public Doctor getDoctorById(Integer doctor_id);
	public void deleteDocotr(Integer doctor_id);
	public List<Doctor> getDoctorByDepartment(Integer dept_id);
	public void isDoctorAvailable(Integer doctor_id);
	public void isDoctorOff(Integer doctor_id);
	public List<Doctor> getAllAvailableDoctors();
	public List<Doctor> getAllUnavailableDoctors();
}

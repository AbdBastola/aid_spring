package com.soft.patientinformationsystem.DAO;

import java.util.List;

import com.soft.patientinformationsystem.model.Role;

public interface RoleDAO {

	public List<Role> getAllRoles();
	public Role getRoleById(Integer role_id);
}

package com.soft.patientinformationsystem.DAO;

import java.util.List;

import com.soft.patientinformationsystem.model.Gender;

public interface GenderDAO {

	public List<Gender> getAllGenders();
	public Gender getGenderById(Integer gender_id);
}

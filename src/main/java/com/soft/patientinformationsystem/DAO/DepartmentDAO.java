package com.soft.patientinformationsystem.DAO;

import java.util.List;

import com.soft.patientinformationsystem.model.Department;

public interface DepartmentDAO {

	public List<Department> getAllDepartments();
	public Department getDepartmentById(Integer id);
}

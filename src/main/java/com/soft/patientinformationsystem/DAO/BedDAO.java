package com.soft.patientinformationsystem.DAO;

import java.util.List;

import com.soft.patientinformationsystem.model.Bed;

public interface BedDAO {

	public void assignBed(Integer patient_id,Integer bed_id);
	public void cancelBed(Integer bed_id);
	public List<Bed> getICUBeds();
	public List<Bed> getGeneralWardBeds();
	public List<Bed> getMaternityWardBeds();
	public List<Bed> getFreeBeds();
	public List<Bed> getOccupiedBeds();
}

package com.soft.patientinformationsystem.ServiceImpl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.soft.patientinformationsystem.Service.UserService;
import com.soft.patientinformationsystem.model.Role;

@Service("userLoginService")
public class UserLoginService implements UserDetailsService{

	@Autowired private UserService userService;
	
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		// TODO Auto-generated method stub
		com.soft.patientinformationsystem.model.User user=userService.findByUsername(username);
		List<GrantedAuthority> authorities=buildUserAuthority(user.getRoles());
		return buildUserForAuthentication(user, authorities);
	}

	private User buildUserForAuthentication(com.soft.patientinformationsystem.model.User user,List<GrantedAuthority> authorities){
		return new User(user.getUsername(), user.getPassword(), true, true, true, true, authorities);
	}
	
	
	private List<GrantedAuthority> buildUserAuthority(List<Role> roles){
		Set<GrantedAuthority> setAuths=new HashSet<GrantedAuthority>();
		
		for(Role userRole:roles){
			setAuths.add(new SimpleGrantedAuthority(userRole.getRole_name()));
		}
		List<GrantedAuthority> result=new ArrayList<GrantedAuthority>(setAuths);
		return result;
	}
}

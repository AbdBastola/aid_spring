package com.soft.patientinformationsystem.ServiceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.soft.patientinformationsystem.DAO.DepartmentDAO;
import com.soft.patientinformationsystem.Service.DepartmentService;
import com.soft.patientinformationsystem.model.Department;

@Service
public class DepartmentServiceImpl implements DepartmentService{

	@Autowired private DepartmentDAO departmentDAO;
	
	@Transactional
	public List<Department> getAllDepartments() {
		// TODO Auto-generated method stub
		return this.departmentDAO.getAllDepartments();
	}

	@Transactional
	public Department getDepartmentById(Integer id) {
		// TODO Auto-generated method stub
		return this.departmentDAO.getDepartmentById(id);
	}

}

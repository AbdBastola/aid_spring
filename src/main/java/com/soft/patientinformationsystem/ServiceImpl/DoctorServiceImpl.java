package com.soft.patientinformationsystem.ServiceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.soft.patientinformationsystem.DAO.DoctorDAO;
import com.soft.patientinformationsystem.Service.DoctorService;
import com.soft.patientinformationsystem.model.Doctor;

@Service
public class DoctorServiceImpl implements DoctorService{

	@Autowired private DoctorDAO doctorDAO;
	
	@Transactional
	public void saveDoctor(Doctor doctor, Integer dept_id) {
		// TODO Auto-generated method stub
		this.doctorDAO.saveDoctor(doctor, dept_id);
	}

	@Transactional
	public void updateDoctor(Doctor doctor, Integer dept_id) {
		// TODO Auto-generated method stub
		this.doctorDAO.updateDoctor(doctor, dept_id);
	}

	@Transactional
	public List<Doctor> getAllDoctors() {
		// TODO Auto-generated method stub
		return this.doctorDAO.getAllDoctors();
	}

	@Transactional
	public Doctor getDoctorById(Integer doctor_id) {
		// TODO Auto-generated method stub
		return this.doctorDAO.getDoctorById(doctor_id);
	}

	@Transactional
	public void deleteDoctor(Integer doctor_id) {
		// TODO Auto-generated method stub
		this.doctorDAO.deleteDocotr(doctor_id);
	}

	@Transactional
	public List<Doctor> getDoctorByDepartment(Integer dept_id) {
		// TODO Auto-generated method stub
		return this.doctorDAO.getDoctorByDepartment(dept_id);
	}

	@Transactional
	public void isDoctorAvailable(Integer doctor_id) {
		// TODO Auto-generated method stub
		this.doctorDAO.isDoctorAvailable(doctor_id);
	}

	@Transactional
	public void isDoctorOff(Integer doctor_id) {
		// TODO Auto-generated method stub
		this.doctorDAO.isDoctorOff(doctor_id);
	}

	public List<Doctor> getAllAvailableDoctors() {
		// TODO Auto-generated method stub
		return this.doctorDAO.getAllAvailableDoctors();
	}

	public List<Doctor> getAllUnAvailableDoctors() {
		// TODO Auto-generated method stub
		return this.doctorDAO.getAllUnavailableDoctors();
	}

	

}

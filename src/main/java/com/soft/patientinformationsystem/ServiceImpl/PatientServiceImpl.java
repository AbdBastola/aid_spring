package com.soft.patientinformationsystem.ServiceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.soft.patientinformationsystem.DAO.PatientDAO;
import com.soft.patientinformationsystem.Service.PatientService;
import com.soft.patientinformationsystem.model.Patient;

@Service
public class PatientServiceImpl implements PatientService{

	@Autowired private PatientDAO patientDAO;
	
	@Transactional
	public void savePatient(Patient patient,Integer gender_id,Integer dept_id,Integer doctor_id) {
		// TODO Auto-generated method stub
		this.patientDAO.savePatient(patient, gender_id, dept_id, doctor_id);
	}

	@Transactional
	public void updatePatient(Patient patient,Integer gender_id,Integer dept_id,Integer doctor_id) {
		// TODO Auto-generated method stub
		this.patientDAO.updatePatient(patient, gender_id, dept_id, doctor_id);
	}

	@Transactional
	public List<Patient> getAllPatients() {
		// TODO Auto-generated method stub
		return this.patientDAO.getAllPatients();
	}

	@Transactional
	public Patient getPatientById(Integer patient_id) {
		// TODO Auto-generated method stub
		return this.patientDAO.getPatientById(patient_id);
	}

	@Transactional
	public void deletePatient(Integer patient_id) {
		// TODO Auto-generated method stub
		this.patientDAO.deletePatient(patient_id);
	}

}

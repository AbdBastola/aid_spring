package com.soft.patientinformationsystem.ServiceImpl;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.soft.patientinformationsystem.DAO.PatientInformationDAO;
import com.soft.patientinformationsystem.Service.PatientInformationService;
import com.soft.patientinformationsystem.model.PatientInformation;

@Service
public class PatientInformationServiceImpl implements PatientInformationService{

	@Autowired private PatientInformationDAO patientInformationDAO;
	
	@Transactional
	public void savePatientInformation(Integer patient_id, PatientInformation patientInformation) {
		// TODO Auto-generated method stub
		this.patientInformationDAO.savePatientInformation(patient_id, patientInformation);
	}

	@Transactional
	public List<PatientInformation> getAllPatientInformation() {
		// TODO Auto-generated method stub
		return this.patientInformationDAO.getAllPatientInformation();
	}

}

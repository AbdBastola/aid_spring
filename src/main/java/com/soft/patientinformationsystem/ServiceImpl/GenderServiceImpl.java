package com.soft.patientinformationsystem.ServiceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.soft.patientinformationsystem.DAO.GenderDAO;
import com.soft.patientinformationsystem.Service.GenderService;
import com.soft.patientinformationsystem.model.Gender;

@Service
public class GenderServiceImpl implements GenderService{

	@Autowired private GenderDAO genderDAO;
	
	@Transactional
	public List<Gender> getAllGenders() {
		// TODO Auto-generated method stub
		return this.genderDAO.getAllGenders();
	}

	@Transactional
	public Gender getGenderById(Integer gender_id) {
		// TODO Auto-generated method stub
		return this.genderDAO.getGenderById(gender_id);
	}

}

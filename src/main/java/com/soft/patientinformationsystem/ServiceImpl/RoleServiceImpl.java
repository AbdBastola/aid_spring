package com.soft.patientinformationsystem.ServiceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.soft.patientinformationsystem.DAO.RoleDAO;
import com.soft.patientinformationsystem.Service.RoleService;
import com.soft.patientinformationsystem.model.Role;

@Service
public class RoleServiceImpl implements RoleService{

	@Autowired private RoleDAO roleDAO;
	
	
	@Transactional
	public List<Role> getAllRoles() {
		// TODO Auto-generated method stub
		return this.roleDAO.getAllRoles();
	}

	@Transactional
	public Role getRoleById(Integer role_id) {
		// TODO Auto-generated method stub
		return this.roleDAO.getRoleById(role_id);
	}

}

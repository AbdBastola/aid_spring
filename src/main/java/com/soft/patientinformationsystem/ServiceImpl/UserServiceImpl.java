package com.soft.patientinformationsystem.ServiceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.soft.patientinformationsystem.DAO.UserDAO;
import com.soft.patientinformationsystem.Service.UserService;
import com.soft.patientinformationsystem.model.User;

@Service
public class UserServiceImpl implements UserService{

	@Autowired private UserDAO userDAO;
	
	@Transactional
	public void saveUser(User user,Integer role_id) {
		// TODO Auto-generated method stub
		this.userDAO.saveUser(user,role_id);
	}

	@Transactional
	public void updateUser(User user,Integer role_id) {
		// TODO Auto-generated method stub
		this.userDAO.updateUser(user,role_id);
	}

	@Transactional
	public List<User> getAllUsers() {
		// TODO Auto-generated method stub
		return this.userDAO.getAllUsers();
	}

	@Transactional
	public User getUserById(Integer id) {
		// TODO Auto-generated method stub
		return this.userDAO.getUserById(id);
	}

	@Transactional
	public void deleteUser(Integer id) {
		// TODO Auto-generated method stub
		this.userDAO.deleteUser(id);
	}

	@Transactional
	public User findByUsername(String username) {
		// TODO Auto-generated method stub
		return this.userDAO.findByUsername(username);
	}

	public boolean isUsernameExists(String username) {
		// TODO Auto-generated method stub
		return this.userDAO.isUsernameExists(username);
	}

}

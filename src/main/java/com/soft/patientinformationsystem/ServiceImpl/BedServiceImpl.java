package com.soft.patientinformationsystem.ServiceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.soft.patientinformationsystem.DAO.BedDAO;
import com.soft.patientinformationsystem.Service.BedService;
import com.soft.patientinformationsystem.model.Bed;

@Service
public class BedServiceImpl implements BedService{

	@Autowired private BedDAO bedDAO;
	
	@Transactional
	public void assignBed(Integer patient_id, Integer bed_id) {
		// TODO Auto-generated method stub
		this.bedDAO.assignBed(patient_id, bed_id);
	}

	@Transactional
	public void cancelBed(Integer bed_id) {
		// TODO Auto-generated method stub
		this.bedDAO.cancelBed(bed_id);
	}
	
	@Transactional
	public List<Bed> getICUBeds() {
		// TODO Auto-generated method stub
		return this.bedDAO.getICUBeds();
	}

	@Transactional
	public List<Bed> getGeneralWardBeds() {
		// TODO Auto-generated method stub
		return this.bedDAO.getGeneralWardBeds();
	}

	@Transactional
	public List<Bed> getMaternityWardBeds() {
		// TODO Auto-generated method stub
		return this.bedDAO.getMaternityWardBeds();
	}

	public List<Bed> getFreebeds() {
		// TODO Auto-generated method stub
		return this.bedDAO.getFreeBeds();
	}

	public List<Bed> getOccupiedBeds() {
		// TODO Auto-generated method stub
		return this.bedDAO.getOccupiedBeds();
	}


	

}

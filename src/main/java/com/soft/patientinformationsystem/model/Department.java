package com.soft.patientinformationsystem.model;

import java.util.ArrayList;
import java.util.List;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;


@Entity
@Table(name="departments")
public class Department {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="dept_id")
	private int dept_id;
	
	@Column(name="department_name")
	private String department_name;
	
	@OneToMany(mappedBy="department",targetEntity=Patient.class,fetch=FetchType.EAGER)
	@JsonBackReference
	private List<Patient> patients=new ArrayList<Patient>();
	
	@OneToMany(mappedBy = "department",fetch=FetchType.EAGER)
	@JsonBackReference
    private List<Doctor> doctorList=new ArrayList<Doctor>();

	public Department(){
		
	}
	
	public Department(int dept_id, String department_name, List<Patient> patients, List<Doctor> doctorList) {
		super();
		this.dept_id = dept_id;
		this.department_name = department_name;
		this.patients = patients;
		this.doctorList = doctorList;
	}



	public int getDept_id() {
		return dept_id;
	}

	public void setDept_id(int dept_id) {
		this.dept_id = dept_id;
	}

	public String getDepartment_name() {
		return department_name;
	}

	public void setDepartment_name(String department_name) {
		this.department_name = department_name;
	}

	public List<Patient> getPatients() {
		return patients;
	}

	public void setPatients(List<Patient> patients) {
		this.patients = patients;
	}

	public List<Doctor> getDoctorList() {
		return doctorList;
	}

	public void setDoctorList(List<Doctor> doctorList) {
		this.doctorList = doctorList;
	}

	@Override
	public String toString() {
		return "Department [dept_id=" + dept_id + ", department_name=" + department_name + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((department_name == null) ? 0 : department_name.hashCode());
		result = prime * result + dept_id;
		result = prime * result + ((doctorList == null) ? 0 : doctorList.hashCode());
		result = prime * result + ((patients == null) ? 0 : patients.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Department other = (Department) obj;
		if (department_name == null) {
			if (other.department_name != null)
				return false;
		} else if (!department_name.equals(other.department_name))
			return false;
		if (dept_id != other.dept_id)
			return false;
		if (doctorList == null) {
			if (other.doctorList != null)
				return false;
		} else if (!doctorList.equals(other.doctorList))
			return false;
		if (patients == null) {
			if (other.patients != null)
				return false;
		} else if (!patients.equals(other.patients))
			return false;
		return true;
	}

	

	
	
	
}

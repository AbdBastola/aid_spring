package com.soft.patientinformationsystem.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name="genders")
public class Gender {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="gender_id")
	private int gender_id;
	
	@Column(name="gender_name")
	private String gender_name;

	@OneToMany(mappedBy="gender",cascade=CascadeType.ALL,fetch=FetchType.EAGER)
	@JsonBackReference
	private List<Patient> patients=new ArrayList<Patient>(); 
	
	public Gender(){
		super();
	}
	
	public Gender(int gender_id, String gender_name) {
		super();
		this.gender_id = gender_id;
		this.gender_name = gender_name;
	}

	public int getGender_id() {
		return gender_id;
	}

	public void setGender_id(int gender_id) {
		this.gender_id = gender_id;
	}

	public String getGender_name() {
		return gender_name;
	}

	public void setGender_name(String gender_name) {
		this.gender_name = gender_name;
	}

	
	
	public List<Patient> getPatients() {
		return patients;
	}

	public void setPatients(List<Patient> patients) {
		this.patients = patients;
	}

	@Override
	public String toString() {
		return "Gender [gender_id=" + gender_id + ", gender_name=" + gender_name + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + gender_id;
		result = prime * result + ((gender_name == null) ? 0 : gender_name.hashCode());
		result = prime * result + ((patients == null) ? 0 : patients.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Gender other = (Gender) obj;
		if (gender_id != other.gender_id)
			return false;
		if (gender_name == null) {
			if (other.gender_name != null)
				return false;
		} else if (!gender_name.equals(other.gender_name))
			return false;
		if (patients == null) {
			if (other.patients != null)
				return false;
		} else if (!patients.equals(other.patients))
			return false;
		return true;
	}
	
	
}

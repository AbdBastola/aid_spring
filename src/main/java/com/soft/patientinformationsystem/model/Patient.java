package com.soft.patientinformationsystem.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name="patients")
public class Patient {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="patient_id")
	private Integer patient_id;
	
	@Column( name="name")
	private String name;
	
	@Column(name="address")
	private String address;
	
	@Column(name="age")
	private int age;
	
	@Column(name="addedDate")
	@Temporal(TemporalType.TIMESTAMP)
	private Date addedDate;
	
	@JoinColumn(name="gender_id", referencedColumnName="gender_id")
	@ManyToOne(fetch=FetchType.EAGER)
	@JsonManagedReference
	private Gender gender;
	
	@JoinColumn(name="dept_id", referencedColumnName="dept_id", nullable=false)
	@ManyToOne(fetch=FetchType.EAGER)
	@JsonManagedReference
	private Department department;
	
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "patient",fetch=FetchType.EAGER)
	@JsonBackReference
    private List<PatientInformation> patientInformationList=new ArrayList<PatientInformation>();
	
	//@JoinColumn(name="bed_no",referencedColumnName = "bed_no")
    //@ManyToOne(fetch=FetchType.EAGER)
    //private Bed bed;
	
	@OneToMany(mappedBy="patient",fetch=FetchType.EAGER,cascade=CascadeType.ALL)
	@JsonBackReference
	private List<Bed> beds=new ArrayList<Bed>();
	
	
	@JoinColumn(name="doctor_id",referencedColumnName="doctor_id")
	@ManyToOne(fetch=FetchType.EAGER)
	@JsonManagedReference
	private Doctor doctor;
	
	public Patient(){
		
	}

	public Patient(Integer patient_id, String name, String address, int age, Date addedDate, Gender gender,
			Department department, List<PatientInformation> patientInformationList, List<Bed> beds, Doctor doctor) {
		super();
		this.patient_id = patient_id;
		this.name = name;
		this.address = address;
		this.age = age;
		this.addedDate = addedDate;
		this.gender = gender;
		this.department = department;
		this.patientInformationList = patientInformationList;
		this.beds = beds;
		this.doctor = doctor;
	}

	public Integer getPatient_id() {
		return patient_id;
	}

	public void setPatient_id(Integer patient_id) {
		this.patient_id = patient_id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public Department getDepartment() {
		return department;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}

	public List<PatientInformation> getPatientInformationList() {
		return patientInformationList;
	}

	public void setPatientInformationList(List<PatientInformation> patientInformationList) {
		this.patientInformationList = patientInformationList;
	}

	
	
	
	public List<Bed> getBeds() {
		return beds;
	}

	public void setBeds(List<Bed> beds) {
		this.beds = beds;
	}

	public Date getAddedDate() {
		return addedDate;
	}

	public void setAddedDate(Date addedDate) {
		this.addedDate = addedDate;
	}

	public Doctor getDoctor() {
		return doctor;
	}

	public void setDoctor(Doctor doctor) {
		this.doctor = doctor;
	}

	
	

	@Override
	public String toString() {
		return "Patient [patient_id=" + patient_id + ", name=" + name + ", address=" + address + ", age=" + age
				+ ", addedDate=" + addedDate + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((addedDate == null) ? 0 : addedDate.hashCode());
		result = prime * result + ((address == null) ? 0 : address.hashCode());
		result = prime * result + age;
		result = prime * result + ((beds == null) ? 0 : beds.hashCode());
		result = prime * result + ((department == null) ? 0 : department.hashCode());
		result = prime * result + ((doctor == null) ? 0 : doctor.hashCode());
		result = prime * result + ((gender == null) ? 0 : gender.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((patientInformationList == null) ? 0 : patientInformationList.hashCode());
		result = prime * result + ((patient_id == null) ? 0 : patient_id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Patient other = (Patient) obj;
		if (addedDate == null) {
			if (other.addedDate != null)
				return false;
		} else if (!addedDate.equals(other.addedDate))
			return false;
		if (address == null) {
			if (other.address != null)
				return false;
		} else if (!address.equals(other.address))
			return false;
		if (age != other.age)
			return false;
		if (beds == null) {
			if (other.beds != null)
				return false;
		} else if (!beds.equals(other.beds))
			return false;
		if (department == null) {
			if (other.department != null)
				return false;
		} else if (!department.equals(other.department))
			return false;
		if (doctor == null) {
			if (other.doctor != null)
				return false;
		} else if (!doctor.equals(other.doctor))
			return false;
		if (gender == null) {
			if (other.gender != null)
				return false;
		} else if (!gender.equals(other.gender))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (patientInformationList == null) {
			if (other.patientInformationList != null)
				return false;
		} else if (!patientInformationList.equals(other.patientInformationList))
			return false;
		if (patient_id == null) {
			if (other.patient_id != null)
				return false;
		} else if (!patient_id.equals(other.patient_id))
			return false;
		return true;
	}



	
	
	
}

package com.soft.patientinformationsystem.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="patientInformations")
public class PatientInformation {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="patientInformationId")
	private int patientInformationId;
	
	
	@Column(length = 65535,name="chiefComplaints")
    private String chiefComplaints;
	
	
    @Column(length = 65535,name="provisionalDiagnosis")
    private String provisionalDiagnosis;
	
    
    @Column(length = 65535,name="clinicalFindings")
    private String clinicalFindings;
    
    @Column(precision = 22,name="weight")
    private Double weight;
    
    @Column(precision = 22,name="height")
    private Double height;
    
    @Column(length = 10,name="bloodPressure")
    private String bloodPressure;
    
    @Column(precision = 22,name="temperature")
    private Double temperature;
    
    @Column(precision = 22,name="pulse")
    private Double pulse;
    
    @JoinColumn(name = "patientId", referencedColumnName = "patient_id", nullable = false)
    @ManyToOne
    private Patient patient;
    
    
    public PatientInformation(){
    	
    }


	public PatientInformation(int patientInformationId, String chiefComplaints, String provisionalDiagnosis,
			String clinicalFindings, Double weight, Double height, String bloodPressure, Double temperature,
			Double pulse, Patient patient) {
		super();
		this.patientInformationId = patientInformationId;
		this.chiefComplaints = chiefComplaints;
		this.provisionalDiagnosis = provisionalDiagnosis;
		this.clinicalFindings = clinicalFindings;
		this.weight = weight;
		this.height = height;
		this.bloodPressure = bloodPressure;
		this.temperature = temperature;
		this.pulse = pulse;
		this.patient = patient;
	}


	public int getPatientInformationId() {
		return patientInformationId;
	}


	public void setPatientInformationId(int patientInformationId) {
		this.patientInformationId = patientInformationId;
	}


	public String getChiefComplaints() {
		return chiefComplaints;
	}


	public void setChiefComplaints(String chiefComplaints) {
		this.chiefComplaints = chiefComplaints;
	}


	public String getProvisionalDiagnosis() {
		return provisionalDiagnosis;
	}


	public void setProvisionalDiagnosis(String provisionalDiagnosis) {
		this.provisionalDiagnosis = provisionalDiagnosis;
	}


	public String getClinicalFindings() {
		return clinicalFindings;
	}


	public void setClinicalFindings(String clinicalFindings) {
		this.clinicalFindings = clinicalFindings;
	}


	public Double getWeight() {
		return weight;
	}


	public void setWeight(Double weight) {
		this.weight = weight;
	}


	public Double getHeight() {
		return height;
	}


	public void setHeight(Double height) {
		this.height = height;
	}


	public String getBloodPressure() {
		return bloodPressure;
	}


	public void setBloodPressure(String bloodPressure) {
		this.bloodPressure = bloodPressure;
	}


	public Double getTemperature() {
		return temperature;
	}


	public void setTemperature(Double temperature) {
		this.temperature = temperature;
	}


	public Double getPulse() {
		return pulse;
	}


	public void setPulse(Double pulse) {
		this.pulse = pulse;
	}


	public Patient getPatient() {
		return patient;
	}


	public void setPatient(Patient patient) {
		this.patient = patient;
	}


	@Override
	public String toString() {
		return "PatientInformation [patientInformationId=" + patientInformationId + ", chiefComplaints="
				+ chiefComplaints + ", provisionalDiagnosis=" + provisionalDiagnosis + ", clinicalFindings="
				+ clinicalFindings + ", weight=" + weight + ", height=" + height + ", bloodPressure=" + bloodPressure
				+ ", temperature=" + temperature + ", pulse=" + pulse + "]";
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((bloodPressure == null) ? 0 : bloodPressure.hashCode());
		result = prime * result + ((chiefComplaints == null) ? 0 : chiefComplaints.hashCode());
		result = prime * result + ((clinicalFindings == null) ? 0 : clinicalFindings.hashCode());
		result = prime * result + ((height == null) ? 0 : height.hashCode());
		result = prime * result + ((patient == null) ? 0 : patient.hashCode());
		result = prime * result + patientInformationId;
		result = prime * result + ((provisionalDiagnosis == null) ? 0 : provisionalDiagnosis.hashCode());
		result = prime * result + ((pulse == null) ? 0 : pulse.hashCode());
		result = prime * result + ((temperature == null) ? 0 : temperature.hashCode());
		result = prime * result + ((weight == null) ? 0 : weight.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PatientInformation other = (PatientInformation) obj;
		if (bloodPressure == null) {
			if (other.bloodPressure != null)
				return false;
		} else if (!bloodPressure.equals(other.bloodPressure))
			return false;
		if (chiefComplaints == null) {
			if (other.chiefComplaints != null)
				return false;
		} else if (!chiefComplaints.equals(other.chiefComplaints))
			return false;
		if (clinicalFindings == null) {
			if (other.clinicalFindings != null)
				return false;
		} else if (!clinicalFindings.equals(other.clinicalFindings))
			return false;
		if (height == null) {
			if (other.height != null)
				return false;
		} else if (!height.equals(other.height))
			return false;
		if (patient == null) {
			if (other.patient != null)
				return false;
		} else if (!patient.equals(other.patient))
			return false;
		if (patientInformationId != other.patientInformationId)
			return false;
		if (provisionalDiagnosis == null) {
			if (other.provisionalDiagnosis != null)
				return false;
		} else if (!provisionalDiagnosis.equals(other.provisionalDiagnosis))
			return false;
		if (pulse == null) {
			if (other.pulse != null)
				return false;
		} else if (!pulse.equals(other.pulse))
			return false;
		if (temperature == null) {
			if (other.temperature != null)
				return false;
		} else if (!temperature.equals(other.temperature))
			return false;
		if (weight == null) {
			if (other.weight != null)
				return false;
		} else if (!weight.equals(other.weight))
			return false;
		return true;
	}
    
    
}

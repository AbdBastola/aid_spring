package com.soft.patientinformationsystem.model;

//imports 
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity  //as a entity
@Table(name="roles")  //name of a table that will be mapped in database
public class Role {

	//id primary key integer mapping to ORM using javax persistance

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="role_id",nullable=false)
	private Integer role_id;
	
	//role_name as a column name mapping to ORM 
	@Column(name="role_name",nullable=false)
	private String role_name;

	//List of users will be mapped by roles present in user class
	@ManyToMany(mappedBy="roles",fetch=FetchType.EAGER,cascade=CascadeType.ALL)  //mapping many to many relationship with user coz many user can have many role
	private List<User> users=new ArrayList<User>();
	
	public Role() {  //default constructor
		super();
	}

	
	//constructor initializing role_id,role_name,List of users
	public Role(Integer role_id, String role_name,List<User> users) {
		super();
		this.role_id = role_id;
		this.role_name = role_name;
		this.users=users;
	}

	//getters and setters
	
	public Integer getRole_id() {
		return role_id;
	}

	public void setRole_id(Integer role_id) {
		this.role_id = role_id;
	}

	public String getRole_name() {
		return role_name;
	}

	public void setRole_name(String role_name) {
		this.role_name = role_name;
	}

	public List<User> getUsers() {
		return users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}

	
	//toString method 
	//never use a user.toString here only because that will cause a never ending loop and results in javax servlet exception but we can use user.toString to know the length of lisT
	@Override
	public String toString() {
		return "Role [role_id=" + role_id + ", role_name=" + role_name +"]";
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((role_id == null) ? 0 : role_id.hashCode());
		result = prime * result + ((role_name == null) ? 0 : role_name.hashCode());
		result = prime * result + ((users == null) ? 0 : users.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Role other = (Role) obj;
		if (role_id == null) {
			if (other.role_id != null)
				return false;
		} else if (!role_id.equals(other.role_id))
			return false;
		if (role_name == null) {
			if (other.role_name != null)
				return false;
		} else if (!role_name.equals(other.role_name))
			return false;
		if (users == null) {
			if (other.users != null)
				return false;
		} else if (!users.equals(other.users))
			return false;
		return true;
	}

	
	
	
	
}

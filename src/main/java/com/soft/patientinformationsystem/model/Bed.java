package com.soft.patientinformationsystem.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name="beds")
public class Bed {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="bed_no")
	private int bed_no;
	
	@Column(name="bedType")
	private String bedType;
	
	@Column(name="bedCharge")
	private Float bedCharge;
	
	@Column(name="status")
	private String status;
	
	//@OneToMany(mappedBy = "bed")
    //private List<Patient> patients=new ArrayList<Patient>();
	
	@JoinColumn(name="patient_id",referencedColumnName="patient_id")
	@ManyToOne(fetch=FetchType.EAGER)
	@JsonManagedReference
	private Patient patient;
	
	public Bed(){
		
	}

	

	public Bed(int bed_no, String bedType, Float bedCharge, String status, Patient patient) {
		super();
		this.bed_no = bed_no;
		this.bedType = bedType;
		this.bedCharge = bedCharge;
		this.status = status;
		this.patient = patient;
	}



	public int getBed_no() {
		return bed_no;
	}

	public void setBed_no(int bed_no) {
		this.bed_no = bed_no;
	}

	public String getBedType() {
		return bedType;
	}

	public void setBedType(String bedType) {
		this.bedType = bedType;
	}

	public Float getBedCharge() {
		return bedCharge;
	}

	public void setBedCharge(Float bedCharge) {
		this.bedCharge = bedCharge;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	

	public Patient getPatient() {
		return patient;
	}



	public void setPatient(Patient patient) {
		this.patient = patient;
	}



	@Override
	public String toString() {
		return "Bed [bed_no=" + bed_no + ", bedType=" + bedType + ", bedCharge=" + bedCharge + ", status=" + status
				+ "]";
	}



	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((bedCharge == null) ? 0 : bedCharge.hashCode());
		result = prime * result + ((bedType == null) ? 0 : bedType.hashCode());
		result = prime * result + bed_no;
		result = prime * result + ((patient == null) ? 0 : patient.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		return result;
	}



	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Bed other = (Bed) obj;
		if (bedCharge == null) {
			if (other.bedCharge != null)
				return false;
		} else if (!bedCharge.equals(other.bedCharge))
			return false;
		if (bedType == null) {
			if (other.bedType != null)
				return false;
		} else if (!bedType.equals(other.bedType))
			return false;
		if (bed_no != other.bed_no)
			return false;
		if (patient == null) {
			if (other.patient != null)
				return false;
		} else if (!patient.equals(other.patient))
			return false;
		if (status == null) {
			if (other.status != null)
				return false;
		} else if (!status.equals(other.status))
			return false;
		return true;
	}
	
	

}

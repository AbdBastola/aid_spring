package com.soft.patientinformationsystem.DAOImpl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.soft.patientinformationsystem.DAO.PatientInformationDAO;
import com.soft.patientinformationsystem.model.Patient;
import com.soft.patientinformationsystem.model.PatientInformation;

@Repository("patientInformationDAO")
public class PatientInformationDAOImpl implements PatientInformationDAO{

	@Autowired private SessionFactory sessionFactory;
	
	private Session session;
	private Transaction trans;
	
	public void savePatientInformation(Integer patient_id,PatientInformation patientInformation) {
		// TODO Auto-generated method stub
		session=sessionFactory.openSession();
		trans=session.beginTransaction();
		
		Patient patient=(Patient)session.get(Patient.class, patient_id);
		patientInformation.setPatient(patient);
		
		session.save(patientInformation);
		trans.commit();
		session.close();
	}

	@SuppressWarnings("unchecked")
	public List<PatientInformation> getAllPatientInformation() {
		// TODO Auto-generated method stub
		session=sessionFactory.openSession();
		trans=session.beginTransaction();
		List<PatientInformation> patientInformations=session.createQuery("FROM PatientInformation pi").list();
		trans.commit();
		session.close();
		return patientInformations;
	}

}

package com.soft.patientinformationsystem.DAOImpl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.soft.patientinformationsystem.DAO.DepartmentDAO;
import com.soft.patientinformationsystem.model.Department;

@Repository("departmentDAO")
public class DepartmentDAOImpl implements DepartmentDAO{

	@Autowired private SessionFactory sessionFactory;
	private Session session;
	private Transaction trans;
	
	@SuppressWarnings("unchecked")
	public List<Department> getAllDepartments() {
		// TODO Auto-generated method stub
		session=sessionFactory.openSession();
		trans=session.beginTransaction();
		List<Department> departments=session.createQuery("FROM Department dept").list();
		if(!trans.wasCommitted()){
			trans.commit();
		}
		session.close();
		return departments;
	}

	public Department getDepartmentById(Integer id) {
		// TODO Auto-generated method stub
		session=sessionFactory.openSession();
		trans=session.beginTransaction();
		Department department=(Department)session.get(Department.class, id);
		trans.commit();
		session.close();
		return department;
	}

}

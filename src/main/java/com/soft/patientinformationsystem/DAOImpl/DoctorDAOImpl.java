package com.soft.patientinformationsystem.DAOImpl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.soft.patientinformationsystem.DAO.DoctorDAO;
import com.soft.patientinformationsystem.model.Department;
import com.soft.patientinformationsystem.model.Doctor;

@Repository("doctorDAO")
public class DoctorDAOImpl implements DoctorDAO{

	@Autowired private SessionFactory sessionFactory;
	private Session session;
	private Transaction trans;
	
	public void saveDoctor(Doctor doctor, Integer dept_id) {
		// TODO Auto-generated method stub
		session=sessionFactory.openSession();
		trans=session.beginTransaction();
		Department dept=(Department)session.get(Department.class, dept_id);
		doctor.setDepartment(dept);
		doctor.setStatus("Unactive");
		session.save(doctor);
		trans.commit();
		session.close();
		
	}

	public void updateDoctor(Doctor doctor, Integer dept_id) {
		// TODO Auto-generated method stub
		session=sessionFactory.openSession();
		trans=session.beginTransaction();
		
		Department dept=(Department)session.get(Department.class, dept_id);
		doctor.setDepartment(dept);
		session.clear();
		
		session.update(doctor);
		trans.commit();
		session.close();
	}

	@SuppressWarnings("unchecked")
	public List<Doctor> getAllDoctors() {
		// TODO Auto-generated method stub
		session=sessionFactory.openSession();
		trans=session.beginTransaction();
		List<Doctor> doctors=session.createQuery("FROM Doctor doctor").list();
		trans.commit();
		session.close();
		return doctors;
	}

	public Doctor getDoctorById(Integer doctor_id) {
		// TODO Auto-generated method stub
		session=sessionFactory.openSession();
		trans=session.beginTransaction();
		Doctor doctor=(Doctor)session.get(Doctor.class, doctor_id);
		trans.commit();
		session.close();
		return doctor;
	}

	public void deleteDocotr(Integer doctor_id) {
		// TODO Auto-generated method stub
		session=sessionFactory.openSession();
		trans=session.beginTransaction();
		Doctor doctor=(Doctor)session.get(Doctor.class, doctor_id);
		session.delete(doctor);
		trans.commit();
		session.close();
	}

	public List<Doctor> getDoctorByDepartment(Integer dept_id) {
		// TODO Auto-generated method stub
		session=sessionFactory.openSession();
		trans=session.beginTransaction();
		@SuppressWarnings("unchecked")
		List<Doctor> doctors=session.createQuery("FROM Doctor d where d.department.dept_id=:dept_id").setInteger("dept_id", dept_id).list();
		trans.commit();
		session.close();
		return doctors;
	}

	public void isDoctorAvailable(Integer doctor_id) {
		// TODO Auto-generated method stub
		session=sessionFactory.openSession();
		trans=session.beginTransaction();
		String queryForDoctorAvailable="UPDATE Doctor doc SET doc.status='active' where doc.doctor_id=:doctor_id";
		Query query=session.createQuery(queryForDoctorAvailable);
		query.setParameter("doctor_id", doctor_id);
		query.executeUpdate();
		trans.commit();
		session.close();
	}

	public void isDoctorOff(Integer doctor_id) {
		// TODO Auto-generated method stub
		session=sessionFactory.openSession();
		trans=session.beginTransaction();
		String queryForDoctorOff="UPDATE Doctor doc SET doc.status='inactive' where doc.doctor_id=:doctor_id";
		Query query=session.createQuery(queryForDoctorOff);
		query.setParameter("doctor_id", doctor_id);
		query.executeUpdate();
		trans.commit();
		session.close();
	}

	@SuppressWarnings("unchecked")
	public List<Doctor> getAllAvailableDoctors() {
		// TODO Auto-generated method stub
		session=sessionFactory.openSession();
		List<Doctor> allAvailableDoctors=session.createQuery("FROM Doctor doctor where doctor.status='active'").list();
		return allAvailableDoctors;
	}

	@SuppressWarnings("unchecked")
	public List<Doctor> getAllUnavailableDoctors() {
		// TODO Auto-generated method stub
		session=sessionFactory.openSession();
		List<Doctor> allUnavailableDoctors=session.createQuery("FROM Doctor doctor where doctor.status='inactive'").list();
		return allUnavailableDoctors;
	}

	
}

package com.soft.patientinformationsystem.DAOImpl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.soft.patientinformationsystem.DAO.GenderDAO;
import com.soft.patientinformationsystem.model.Gender;

@Repository("genderDAO")
public class GenderDAOImpl implements GenderDAO{

	@Autowired private SessionFactory sessionFactory;
			   private Session session;
			   private Transaction trans;
	
	@SuppressWarnings("unchecked")
	public List<Gender> getAllGenders() {
		// TODO Auto-generated method stub
		session=sessionFactory.openSession();
		trans=session.beginTransaction();
		List<Gender>genders=session.createQuery("FROM Gender gender").list();
		trans.commit();
		session.close();
		return genders;
	}

	public Gender getGenderById(Integer gender_id) {
		// TODO Auto-generated method stub
		session=sessionFactory.openSession();
		trans=session.beginTransaction();
		Gender gender=(Gender)session.get(Gender.class, gender_id);
		trans.commit();
		session.close();
		return gender;
	}

}

package com.soft.patientinformationsystem.DAOImpl;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.soft.patientinformationsystem.DAO.UserDAO;
import com.soft.patientinformationsystem.model.Role;
import com.soft.patientinformationsystem.model.User;

@Repository("userDAO")
public class UserDAOImpl implements UserDAO{

	@Autowired
	private SessionFactory sessionFactory;
	private Session session;
	private Transaction trans;
	private final Logger log=Logger.getLogger(User.class);
	
	public void saveUser(User user,Integer role_id) {
		// TODO Auto-generated method stub
		session=sessionFactory.openSession();
		trans=session.beginTransaction();
		Role role=(Role)session.get(Role.class, role_id);
		user.getRoles().add(role);
		session.save(user);
		log.info("User is saved "+user);
		trans.commit();
		session.close();
	}

	public void updateUser(User user,Integer role_id) {
		// TODO Auto-generated method stub
		session=sessionFactory.openSession();
		trans=session.beginTransaction();
		Role role=(Role)session.get(Role.class, role_id);
		user.getRoles().add(role);
		session.clear();  //clearing the session of roles
		session.update(user);
		log.info("User is updated "+user);
		trans.commit();
		session.close();
	}

	@SuppressWarnings("unchecked")
	public List<User> getAllUsers() {
		// TODO Auto-generated method stub
		session=sessionFactory.openSession();
		trans=session.beginTransaction();
		List<User> users=session.createQuery("FROM User user").list();
		log.info("All users are listed "+users);
		trans.commit();
		session.close();
		return users;
	}

	public User getUserById(Integer id) {
		// TODO Auto-generated method stub
		session=sessionFactory.openSession();
		trans=session.beginTransaction();
		User user=(User) session.get(User.class, id);
		log.info("User is retrieved according to id "+user);
		trans.commit();
		session.close();
		return user;
	}

	public void deleteUser(Integer id) {
		// TODO Auto-generated method stub
		session=sessionFactory.openSession();
		trans=session.beginTransaction();
		User user=(User) session.get(User.class, id);
		log.info("User is deleted "+user);
		session.delete(user);
		trans.commit();
		session.close();
	}

	public User findByUsername(String username) {
		// TODO Auto-generated method stub
		session=sessionFactory.openSession();
		trans=session.beginTransaction();
		@SuppressWarnings("unchecked")
		List<User> user=session.createQuery("FROM User where username=:username").setString("username", username).list();
		if(user.size()>0){
			return user.get(0);
		}else{
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	public boolean isUsernameExists(String username) {
		// TODO Auto-generated method stub
		session=sessionFactory.openSession();
		trans=session.beginTransaction();
		List<String> allUsername=session.createQuery("SELECT username FROM User user").list();
		if(allUsername.contains(username)){
			return false;
		}else{
			return true;
		}
		
	}

}

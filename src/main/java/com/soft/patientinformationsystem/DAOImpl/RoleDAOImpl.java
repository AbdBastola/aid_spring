package com.soft.patientinformationsystem.DAOImpl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.soft.patientinformationsystem.DAO.RoleDAO;
import com.soft.patientinformationsystem.model.Role;

@Repository("roleDAO")
public class RoleDAOImpl implements RoleDAO{

	@Autowired
	private SessionFactory sessionFactory;
	private Transaction trans;
	private Session session;
	
	@SuppressWarnings("unchecked")
	public List<Role> getAllRoles() {
		// TODO Auto-generated method stub
		session=sessionFactory.openSession();
		trans=session.beginTransaction();
		List<Role> roles=session.createQuery("FROM Role role").list();
		trans.commit();
		session.close();
		return roles;
	}

	public Role getRoleById(Integer role_id) {
		// TODO Auto-generated method stub
		session=sessionFactory.openSession();
		trans=session.beginTransaction();
		Role role=(Role)session.get(Role.class, role_id);
		trans.commit();
		session.close();
		return role;
	}

}

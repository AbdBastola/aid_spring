package com.soft.patientinformationsystem.DAOImpl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.soft.patientinformationsystem.DAO.BedDAO;
import com.soft.patientinformationsystem.model.Bed;


@Repository("bedDAO")
public class BedDAOImpl implements BedDAO{

	@Autowired private SessionFactory sessionFactory;
	
	private Session session;
	private Transaction trans;
	
	public void assignBed(Integer patient_id, Integer bed_id) {
		// TODO Auto-generated method stub
		session=sessionFactory.openSession();
		trans=session.beginTransaction();
		String updatequery="UPDATE Bed bed set bed.patient.patient_id=:patient_id,bed.status='occupied' where bed.bed_no=:bed_id";
		Query query=session.createQuery(updatequery);
		query.setParameter("patient_id", patient_id);
		query.setParameter("bed_id", bed_id);
		query.executeUpdate();
		trans.commit();
		session.close();
	}

	

	public void cancelBed(Integer bed_id) {
		// TODO Auto-generated method stub
		session=sessionFactory.openSession();
		trans=session.beginTransaction();
		String cancelBedQuery="UPDATE Bed bed set bed.status='free' where bed.bed_no=:bed_id";
		Query query=session.createQuery(cancelBedQuery);
		query.setParameter("bed_id", bed_id);
		query.executeUpdate();
		trans.commit();
		session.close();
	}
	
	
	@SuppressWarnings("unchecked")
	public List<Bed> getICUBeds() {
		// TODO Auto-generated method stub
		session=sessionFactory.openSession();
		List<Bed> icuBeds=session.createQuery("FROM Bed bed where bed.bedType='ICU'").list();
		return icuBeds;
	}

	@SuppressWarnings("unchecked")
	public List<Bed> getGeneralWardBeds() {
		// TODO Auto-generated method stub
		session=sessionFactory.openSession();
		List<Bed> generalWardBeds=session.createQuery("FROM Bed bed where bed.bedType='General ward'").list();
		return generalWardBeds;
	}
	
	@SuppressWarnings("unchecked")
	public List<Bed> getMaternityWardBeds() {
		// TODO Auto-generated method stub
		session=sessionFactory.openSession();
		List<Bed> maternityWardBeds=session.createQuery("FROM Bed bed where bed.bedType='Maternity Ward'").list();
		return maternityWardBeds;
	}

	@SuppressWarnings("unchecked")
	public List<Bed> getFreeBeds() {
		// TODO Auto-generated method stub
		session=sessionFactory.openSession();
		List<Bed> freeBeds=session.createQuery("FROM Bed bed where bed.status='free'").list();
		return freeBeds;
	}

	@SuppressWarnings("unchecked")
	public List<Bed> getOccupiedBeds() {
		// TODO Auto-generated method stub
		session=sessionFactory.openSession();
		List<Bed> occupiedBeds=session.createQuery("FROM Bed bed where bed.status='occupied'").list();
		return occupiedBeds;
	}




}

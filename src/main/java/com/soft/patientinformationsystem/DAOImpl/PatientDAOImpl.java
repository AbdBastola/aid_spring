package com.soft.patientinformationsystem.DAOImpl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.soft.patientinformationsystem.DAO.PatientDAO;
import com.soft.patientinformationsystem.model.Department;
import com.soft.patientinformationsystem.model.Doctor;
import com.soft.patientinformationsystem.model.Gender;
import com.soft.patientinformationsystem.model.Patient;

@Repository("patientDAO")
public class PatientDAOImpl implements PatientDAO{

	@Autowired private SessionFactory sessionFactory;
	private Session session;
	private Transaction trans;
	
	public void savePatient(Patient patient,Integer gender_id,Integer dept_id,Integer doctor_id) {
		// TODO Auto-generated method stub
		session=sessionFactory.openSession();
		trans=session.beginTransaction();
		
		Gender gender=(Gender)session.get(Gender.class, gender_id);
		patient.setGender(gender);
		
		Department dept=(Department)session.get(Department.class, dept_id);
		patient.setDepartment(dept);
		
		Doctor doctor=(Doctor)session.get(Doctor.class, doctor_id);
		patient.setDoctor(doctor);
		
		session.save(patient);
		session.flush();
		trans.commit();
		session.close();
	}

	public void updatePatient(Patient patient,Integer gender_id,Integer dept_id,Integer doctor_id) {
		// TODO Auto-generated method stub
		
		session=sessionFactory.openSession();
		trans=session.beginTransaction();
		Gender gender=(Gender)session.get(Gender.class, gender_id);
		patient.setGender(gender);
		session.clear();
		
		Department department=(Department)session.get(Department.class, dept_id);
		patient.setDepartment(department);
		session.clear();
		
		Doctor doctor=(Doctor)session.get(Doctor.class, doctor_id);
		patient.setDoctor(doctor);
		session.clear();
		
		session.update(patient);
		trans.commit();
		session.close();
	}

	@SuppressWarnings("unchecked")
	public List<Patient> getAllPatients() {
		// TODO Auto-generated method stub
		session=sessionFactory.openSession();
		trans=session.beginTransaction();
		List<Patient> patients=session.createQuery("FROM Patient patient").list();
		trans.commit();
		session.close();
		return patients;
	}

	public Patient getPatientById(Integer patient_id) {
		// TODO Auto-generated method stub
		session=sessionFactory.openSession();
		trans=session.beginTransaction();
		Patient patient=(Patient)session.get(Patient.class, patient_id);
		trans.commit();
		session.close();
		return patient;
	}

	public void deletePatient(Integer patient_id) {
		// TODO Auto-generated method stub
		session=sessionFactory.openSession();
		trans=session.beginTransaction();
		Patient patient=(Patient)session.get(Patient.class, patient_id);
		session.delete(patient);
		trans.commit();
		session.close();
	}

}

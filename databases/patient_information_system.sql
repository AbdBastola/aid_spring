-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 23, 2017 at 04:47 पूर्वाह्न
-- Server version: 10.1.19-MariaDB
-- PHP Version: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `patient_information_system`
--

-- --------------------------------------------------------

--
-- Table structure for table `beds`
--

CREATE TABLE `beds` (
  `bed_no` int(11) NOT NULL,
  `bedCharge` float DEFAULT NULL,
  `bedType` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `patient_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `beds`
--

INSERT INTO `beds` (`bed_no`, `bedCharge`, `bedType`, `status`, `patient_id`) VALUES
(1, 300, 'ICU', 'occupied', 2),
(2, 200, 'General Ward', 'occupied', NULL),
(3, 150, 'Maternity Ward', 'free', NULL),
(4, 300, 'ICU', 'occupied', 3),
(5, 300, 'ICU', 'free', NULL),
(6, 200, 'General ward', 'free', NULL),
(7, 200, 'General Ward', 'free', 13),
(8, 200, 'General Ward', 'free', NULL),
(9, 150, 'Maternity Ward', 'occupied', 1),
(10, 150, 'Maternity Ward', 'free', NULL),
(11, 200, 'General Ward', 'free', NULL),
(12, 200, 'General Ward', 'free', 7),
(13, 150, 'Maternity Ward', 'free', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

CREATE TABLE `departments` (
  `dept_id` int(11) NOT NULL,
  `department_name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `departments`
--

INSERT INTO `departments` (`dept_id`, `department_name`) VALUES
(1, 'Internal Medicine'),
(2, 'Family Medicine'),
(3, 'General Surgery'),
(4, 'Orthopedic Surgery'),
(5, 'Urology'),
(6, 'Gynecology'),
(7, 'Dental Surgery'),
(8, 'ENT'),
(9, 'Cardiology'),
(10, 'Neurology');

-- --------------------------------------------------------

--
-- Table structure for table `doctors`
--

CREATE TABLE `doctors` (
  `doctor_id` int(11) NOT NULL,
  `arrivalTime` varchar(25) DEFAULT NULL,
  `degree` varchar(50) DEFAULT NULL,
  `departureTime` varchar(25) DEFAULT NULL,
  `firstname` varchar(50) DEFAULT NULL,
  `lastname` varchar(50) DEFAULT NULL,
  `specilization` varchar(50) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `deptId` int(11) NOT NULL,
  `addedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `doctors`
--

INSERT INTO `doctors` (`doctor_id`, `arrivalTime`, `degree`, `departureTime`, `firstname`, `lastname`, `specilization`, `status`, `deptId`, `addedDate`) VALUES
(2, '9:00 am', 'MBBS', '3:00 pm', 'Pawan', 'thapa', 'Surgery', 'inactive', 9, '2017-04-09 18:15:00'),
(4, '7:00 am', 'MBBS', '5:00 pm', 'Abhash', 'Khadka', 'Surgery', 'active', 6, '2017-04-06 06:22:19'),
(6, '7:00 am', 'MD', '7:00 pm', 'Haseeb', 'A.Lone', 'Surgery', 'active', 6, '2017-04-10 10:20:19'),
(7, '5:00 p.m', 'MD in bypass Surgery', '11:00 p.m', 'D.N', 'Asthana', 'Surgery', 'Unactive', 9, '2017-04-10 10:20:19'),
(8, '9:00 am', 'MBBS MD', '12:00 pm', 'KavyaShree', 'Karki', 'General Medicine', 'Unactive', 2, '2017-04-10 10:20:19'),
(9, '12:00 pm', 'MD MBBS', '5 :00 pm', 'Aadhyaya', 'Khadka', 'Neurology', 'active', 10, '2017-04-10 10:41:02');

-- --------------------------------------------------------

--
-- Table structure for table `genders`
--

CREATE TABLE `genders` (
  `gender_id` int(11) NOT NULL,
  `gender_name` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `genders`
--

INSERT INTO `genders` (`gender_id`, `gender_name`) VALUES
(1, 'Male'),
(2, 'Female'),
(3, 'Others');

-- --------------------------------------------------------

--
-- Table structure for table `patientInformations`
--

CREATE TABLE `patientInformations` (
  `patientInformationId` int(11) NOT NULL,
  `bloodPressure` varchar(10) DEFAULT NULL,
  `chiefComplaints` longtext,
  `clinicalFindings` longtext,
  `height` double DEFAULT NULL,
  `provisionalDiagnosis` longtext,
  `pulse` double DEFAULT NULL,
  `temperature` double DEFAULT NULL,
  `weight` double DEFAULT NULL,
  `patientId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `patientInformations`
--

INSERT INTO `patientInformations` (`patientInformationId`, `bloodPressure`, `chiefComplaints`, `clinicalFindings`, `height`, `provisionalDiagnosis`, `pulse`, `temperature`, `weight`, `patientId`) VALUES
(1, '120/60', 'headache, diarrohea', 'yoo oyo yoy', 5, 'HIV positive', 34, 98, 60, 1),
(2, '120/50', 'headche stomache', 'hkhjkhkjxhcvkjbhkx', 4, 'kfsdlkdjlk', 120, 37, 80, 13);

-- --------------------------------------------------------

--
-- Table structure for table `patients`
--

CREATE TABLE `patients` (
  `patient_id` int(11) NOT NULL,
  `address` varchar(255) DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `dept_id` int(11) NOT NULL,
  `doctor_id` int(11) DEFAULT NULL,
  `addedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `gender_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `patients`
--

INSERT INTO `patients` (`patient_id`, `address`, `age`, `name`, `dept_id`, `doctor_id`, `addedDate`, `gender_id`) VALUES
(1, 'Lazimpat,Kathmandu', 30, 'Aakash Tamang', 6, 4, '2017-04-04 12:55:10', 3),
(2, 'Lazimpat', 29, 'Amir Maharjan', 6, 4, '2017-03-31 10:42:15', 1),
(3, 'Kavre,NP', 40, 'Ram Thapa', 1, 2, '2017-03-28 06:22:21', 1),
(4, 'Kavre', 30, 'Hari Bahadur', 9, 2, '2017-03-28 06:22:21', 1),
(5, 'Kathmandu', 34, 'Aayush Maharjan', 1, 2, '2017-03-28 06:43:01', 1),
(6, 'kathmandu', 20, 'Aaditya Baral', 3, 2, '2017-03-29 11:47:49', 2),
(7, 'kathmandu,NP', 19, 'Pukar Thapa', 10, 2, '2017-03-29 11:49:42', 1),
(8, 'kathmandu,NP', 34, 'RAM PRASHAD ', 3, 4, '2017-03-29 11:55:38', 1),
(9, 'kathmandu', 30, 'DAMODAR BARAL', 9, 2, '2017-03-31 03:25:36', 1),
(10, 'Bhaktapur', 20, 'RANJIB GAUTAM', 9, 2, '2017-03-31 10:45:44', 2),
(11, 'Bhaktapur', 20, 'RANJAN KARKI', 9, 2, '2017-03-31 09:31:12', 1),
(12, 'kathmandu', 21, 'Pawone', 9, 2, '2017-04-08 18:35:43', 1),
(13, 'kathmandu', 34, 'Jigar Gurung', 9, 7, '2017-04-09 03:05:09', 1),
(14, 'Baneshwor', 8, 'Adhyaya Khadka', 9, 2, '2017-04-10 09:26:06', 1),
(15, 'Kavre,NP', 23, 'Pradeep Thapa', 2, 8, '2017-04-10 10:36:08', 1);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `role_id` int(11) NOT NULL,
  `role_name` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`role_id`, `role_name`) VALUES
(1, 'ROLE_ADMIN'),
(5, 'ROLE_LABTECHNICIAN'),
(6, 'ROLE_DOCTOR'),
(7, 'ROLE_FRONTDESKSTAFFS');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `address` varchar(255) DEFAULT NULL,
  `firstname` varchar(255) DEFAULT NULL,
  `lastname` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `address`, `firstname`, `lastname`, `password`, `username`) VALUES
(1, 'kathmandu,NP', 'Pawan', 'Thapa', 'pawan123', 'pawanthapa@gmail.com'),
(2, 'Kavre,NP', 'Pukar', 'Thapa', 'pukar123', 'pukar@gmail.com'),
(3, 'Nakhipot,NP', 'Abhash', 'Khadka', 'abhash123', 'abhash@gmail.com'),
(6, 'Kathmandu,NP', 'Pawan', 'Thapa', 'pawan123', 'pawan123'),
(7, 'Lazimpat', 'Amir', 'Motey', 'motey1234', 'motey6ka'),
(8, 'kathmandu', 'Achyut', 'Timilsina', 'achyut123', 'achyuttimsina@gmail.com'),
(9, 'kathmandu,NP', 'Pawone', 'Thapa', 'paw127', 't7pawan@gmail.com'),
(10, 'Kavre,NP', 'PawAN', 'thAPA', 'paw127', 't127pawan@gmail.com'),
(11, 'Bhaktapur', 'KavyaShree', 'Karki', 'kavya123', 'kavya@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `user_roles`
--

CREATE TABLE `user_roles` (
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_roles`
--

INSERT INTO `user_roles` (`user_id`, `role_id`) VALUES
(6, 1),
(2, 7),
(3, 6),
(1, 1),
(7, 7),
(8, 1),
(9, 7),
(10, 6),
(11, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `beds`
--
ALTER TABLE `beds`
  ADD PRIMARY KEY (`bed_no`),
  ADD KEY `FK_sa66abo1oh5r9wise9hpsut23` (`patient_id`);

--
-- Indexes for table `departments`
--
ALTER TABLE `departments`
  ADD PRIMARY KEY (`dept_id`);

--
-- Indexes for table `doctors`
--
ALTER TABLE `doctors`
  ADD PRIMARY KEY (`doctor_id`),
  ADD KEY `FK_el9we7ip5x0y6c5l504fmh2tl` (`deptId`);

--
-- Indexes for table `genders`
--
ALTER TABLE `genders`
  ADD PRIMARY KEY (`gender_id`);

--
-- Indexes for table `patientInformations`
--
ALTER TABLE `patientInformations`
  ADD PRIMARY KEY (`patientInformationId`),
  ADD KEY `FK_qmd0hnslx455gxhfkj1wgtfhp` (`patientId`);

--
-- Indexes for table `patients`
--
ALTER TABLE `patients`
  ADD PRIMARY KEY (`patient_id`),
  ADD KEY `FK_mo6o6sim9fmgaksprl0ooiwos` (`dept_id`),
  ADD KEY `FK_jjxgk8c44ysfjturw986ofk12` (`doctor_id`),
  ADD KEY `FK_5u81gma9lni01uqgu8dlllkex` (`gender_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`role_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD KEY `FK_5q4rc4fh1on6567qk69uesvyf` (`role_id`),
  ADD KEY `FK_g1uebn6mqk9qiaw45vnacmyo2` (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `beds`
--
ALTER TABLE `beds`
  MODIFY `bed_no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `departments`
--
ALTER TABLE `departments`
  MODIFY `dept_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `doctors`
--
ALTER TABLE `doctors`
  MODIFY `doctor_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `genders`
--
ALTER TABLE `genders`
  MODIFY `gender_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `patientInformations`
--
ALTER TABLE `patientInformations`
  MODIFY `patientInformationId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `patients`
--
ALTER TABLE `patients`
  MODIFY `patient_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `role_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `beds`
--
ALTER TABLE `beds`
  ADD CONSTRAINT `FK_sa66abo1oh5r9wise9hpsut23` FOREIGN KEY (`patient_id`) REFERENCES `patients` (`patient_id`);

--
-- Constraints for table `doctors`
--
ALTER TABLE `doctors`
  ADD CONSTRAINT `FK_el9we7ip5x0y6c5l504fmh2tl` FOREIGN KEY (`deptId`) REFERENCES `departments` (`dept_id`);

--
-- Constraints for table `patientInformations`
--
ALTER TABLE `patientInformations`
  ADD CONSTRAINT `FK_qmd0hnslx455gxhfkj1wgtfhp` FOREIGN KEY (`patientId`) REFERENCES `patients` (`patient_id`);

--
-- Constraints for table `patients`
--
ALTER TABLE `patients`
  ADD CONSTRAINT `FK_5u81gma9lni01uqgu8dlllkex` FOREIGN KEY (`gender_id`) REFERENCES `genders` (`gender_id`),
  ADD CONSTRAINT `FK_jjxgk8c44ysfjturw986ofk12` FOREIGN KEY (`doctor_id`) REFERENCES `doctors` (`doctor_id`),
  ADD CONSTRAINT `FK_mo6o6sim9fmgaksprl0ooiwos` FOREIGN KEY (`dept_id`) REFERENCES `departments` (`dept_id`);

--
-- Constraints for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD CONSTRAINT `FK_5q4rc4fh1on6567qk69uesvyf` FOREIGN KEY (`role_id`) REFERENCES `roles` (`role_id`),
  ADD CONSTRAINT `FK_g1uebn6mqk9qiaw45vnacmyo2` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
